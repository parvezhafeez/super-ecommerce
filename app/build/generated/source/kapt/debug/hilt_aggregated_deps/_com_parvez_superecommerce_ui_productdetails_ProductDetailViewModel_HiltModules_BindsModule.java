package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;
import javax.annotation.processing.Generated;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "com.parvez.superecommerce.ui.productdetails.ProductDetailViewModel_HiltModules.BindsModule"
)
@Generated("dagger.hilt.processor.internal.aggregateddeps.AggregatedDepsGenerator")
public class _com_parvez_superecommerce_ui_productdetails_ProductDetailViewModel_HiltModules_BindsModule {
}
