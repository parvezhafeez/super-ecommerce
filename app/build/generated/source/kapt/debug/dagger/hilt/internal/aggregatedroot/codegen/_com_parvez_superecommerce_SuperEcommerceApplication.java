package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;
import javax.annotation.processing.Generated;

/**
 * This class should only be referenced by generated code! This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "com.parvez.superecommerce.SuperEcommerceApplication",
    rootPackage = "com.parvez.superecommerce",
    originatingRoot = "com.parvez.superecommerce.SuperEcommerceApplication",
    originatingRootPackage = "com.parvez.superecommerce",
    rootAnnotation = HiltAndroidApp.class,
    rootSimpleNames = "SuperEcommerceApplication",
    originatingRootSimpleNames = "SuperEcommerceApplication"
)
@Generated("dagger.hilt.processor.internal.root.AggregatedRootGenerator")
public class _com_parvez_superecommerce_SuperEcommerceApplication {
}
