package com.parvez.superecommerce.data.repository.cart.remote;

import com.parvez.superecommerce.data.remote.ApiInterface;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CartRemoteSourceImpl_Factory implements Factory<CartRemoteSourceImpl> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public CartRemoteSourceImpl_Factory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public CartRemoteSourceImpl get() {
    return newInstance(apiInterfaceProvider.get());
  }

  public static CartRemoteSourceImpl_Factory create(Provider<ApiInterface> apiInterfaceProvider) {
    return new CartRemoteSourceImpl_Factory(apiInterfaceProvider);
  }

  public static CartRemoteSourceImpl newInstance(ApiInterface apiInterface) {
    return new CartRemoteSourceImpl(apiInterface);
  }
}
