package com.parvez.superecommerce.ui.home;

import com.parvez.superecommerce.domain.usecase.GetCategoriesUseCase;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class HomeViewModel_Factory implements Factory<HomeViewModel> {
  private final Provider<GetCategoriesUseCase> getCategoriesUseCaseProvider;

  public HomeViewModel_Factory(Provider<GetCategoriesUseCase> getCategoriesUseCaseProvider) {
    this.getCategoriesUseCaseProvider = getCategoriesUseCaseProvider;
  }

  @Override
  public HomeViewModel get() {
    return newInstance(getCategoriesUseCaseProvider.get());
  }

  public static HomeViewModel_Factory create(
      Provider<GetCategoriesUseCase> getCategoriesUseCaseProvider) {
    return new HomeViewModel_Factory(getCategoriesUseCaseProvider);
  }

  public static HomeViewModel newInstance(GetCategoriesUseCase getCategoriesUseCase) {
    return new HomeViewModel(getCategoriesUseCase);
  }
}
