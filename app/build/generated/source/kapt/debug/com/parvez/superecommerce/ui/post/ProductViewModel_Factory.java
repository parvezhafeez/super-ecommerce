package com.parvez.superecommerce.ui.post;

import androidx.lifecycle.SavedStateHandle;
import com.parvez.superecommerce.domain.usecase.GetPostUseCase;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ProductViewModel_Factory implements Factory<ProductViewModel> {
  private final Provider<SavedStateHandle> savedStateHandleProvider;

  private final Provider<GetPostUseCase> getPostUseCaseProvider;

  public ProductViewModel_Factory(Provider<SavedStateHandle> savedStateHandleProvider,
      Provider<GetPostUseCase> getPostUseCaseProvider) {
    this.savedStateHandleProvider = savedStateHandleProvider;
    this.getPostUseCaseProvider = getPostUseCaseProvider;
  }

  @Override
  public ProductViewModel get() {
    return newInstance(savedStateHandleProvider.get(), getPostUseCaseProvider.get());
  }

  public static ProductViewModel_Factory create(Provider<SavedStateHandle> savedStateHandleProvider,
      Provider<GetPostUseCase> getPostUseCaseProvider) {
    return new ProductViewModel_Factory(savedStateHandleProvider, getPostUseCaseProvider);
  }

  public static ProductViewModel newInstance(SavedStateHandle savedStateHandle,
      GetPostUseCase getPostUseCase) {
    return new ProductViewModel(savedStateHandle, getPostUseCase);
  }
}
