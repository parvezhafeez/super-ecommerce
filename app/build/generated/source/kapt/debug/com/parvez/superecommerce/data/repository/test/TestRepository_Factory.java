package com.parvez.superecommerce.data.repository.test;

import com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TestRepository_Factory implements Factory<TestRepository> {
  private final Provider<TestRemoteSource> testRemoteSourceProvider;

  public TestRepository_Factory(Provider<TestRemoteSource> testRemoteSourceProvider) {
    this.testRemoteSourceProvider = testRemoteSourceProvider;
  }

  @Override
  public TestRepository get() {
    return newInstance(testRemoteSourceProvider.get());
  }

  public static TestRepository_Factory create(Provider<TestRemoteSource> testRemoteSourceProvider) {
    return new TestRepository_Factory(testRemoteSourceProvider);
  }

  public static TestRepository newInstance(TestRemoteSource testRemoteSource) {
    return new TestRepository(testRemoteSource);
  }
}
