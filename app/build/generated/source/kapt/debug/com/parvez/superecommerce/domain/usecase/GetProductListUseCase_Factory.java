package com.parvez.superecommerce.domain.usecase;

import com.parvez.superecommerce.data.repository.product.ProductRepository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetProductListUseCase_Factory implements Factory<GetProductListUseCase> {
  private final Provider<ProductRepository> productRepositoryProvider;

  public GetProductListUseCase_Factory(Provider<ProductRepository> productRepositoryProvider) {
    this.productRepositoryProvider = productRepositoryProvider;
  }

  @Override
  public GetProductListUseCase get() {
    return newInstance(productRepositoryProvider.get());
  }

  public static GetProductListUseCase_Factory create(
      Provider<ProductRepository> productRepositoryProvider) {
    return new GetProductListUseCase_Factory(productRepositoryProvider);
  }

  public static GetProductListUseCase newInstance(ProductRepository productRepository) {
    return new GetProductListUseCase(productRepository);
  }
}
