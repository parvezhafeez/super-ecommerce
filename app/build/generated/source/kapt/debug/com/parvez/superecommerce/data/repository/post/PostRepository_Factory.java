package com.parvez.superecommerce.data.repository.post;

import com.parvez.superecommerce.data.repository.post.remote.PostRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PostRepository_Factory implements Factory<PostRepository> {
  private final Provider<PostRemoteSource> postRemoteSourceProvider;

  public PostRepository_Factory(Provider<PostRemoteSource> postRemoteSourceProvider) {
    this.postRemoteSourceProvider = postRemoteSourceProvider;
  }

  @Override
  public PostRepository get() {
    return newInstance(postRemoteSourceProvider.get());
  }

  public static PostRepository_Factory create(Provider<PostRemoteSource> postRemoteSourceProvider) {
    return new PostRepository_Factory(postRemoteSourceProvider);
  }

  public static PostRepository newInstance(PostRemoteSource postRemoteSource) {
    return new PostRepository(postRemoteSource);
  }
}
