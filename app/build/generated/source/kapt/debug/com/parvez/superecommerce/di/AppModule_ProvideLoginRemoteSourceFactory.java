package com.parvez.superecommerce.di;

import com.parvez.superecommerce.data.remote.ApiInterface;
import com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvideLoginRemoteSourceFactory implements Factory<LoginRemoteSource> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public AppModule_ProvideLoginRemoteSourceFactory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public LoginRemoteSource get() {
    return provideLoginRemoteSource(apiInterfaceProvider.get());
  }

  public static AppModule_ProvideLoginRemoteSourceFactory create(
      Provider<ApiInterface> apiInterfaceProvider) {
    return new AppModule_ProvideLoginRemoteSourceFactory(apiInterfaceProvider);
  }

  public static LoginRemoteSource provideLoginRemoteSource(ApiInterface apiInterface) {
    return Preconditions.checkNotNullFromProvides(AppModule.INSTANCE.provideLoginRemoteSource(apiInterface));
  }
}
