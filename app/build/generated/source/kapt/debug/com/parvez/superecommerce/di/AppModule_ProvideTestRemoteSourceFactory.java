package com.parvez.superecommerce.di;

import com.parvez.superecommerce.data.remote.ApiInterface;
import com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvideTestRemoteSourceFactory implements Factory<TestRemoteSource> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public AppModule_ProvideTestRemoteSourceFactory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public TestRemoteSource get() {
    return provideTestRemoteSource(apiInterfaceProvider.get());
  }

  public static AppModule_ProvideTestRemoteSourceFactory create(
      Provider<ApiInterface> apiInterfaceProvider) {
    return new AppModule_ProvideTestRemoteSourceFactory(apiInterfaceProvider);
  }

  public static TestRemoteSource provideTestRemoteSource(ApiInterface apiInterface) {
    return Preconditions.checkNotNullFromProvides(AppModule.INSTANCE.provideTestRemoteSource(apiInterface));
  }
}
