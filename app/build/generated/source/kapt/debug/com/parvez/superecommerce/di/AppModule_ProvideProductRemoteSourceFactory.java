package com.parvez.superecommerce.di;

import com.parvez.superecommerce.data.remote.ApiInterface;
import com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvideProductRemoteSourceFactory implements Factory<ProductRemoteSource> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public AppModule_ProvideProductRemoteSourceFactory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public ProductRemoteSource get() {
    return provideProductRemoteSource(apiInterfaceProvider.get());
  }

  public static AppModule_ProvideProductRemoteSourceFactory create(
      Provider<ApiInterface> apiInterfaceProvider) {
    return new AppModule_ProvideProductRemoteSourceFactory(apiInterfaceProvider);
  }

  public static ProductRemoteSource provideProductRemoteSource(ApiInterface apiInterface) {
    return Preconditions.checkNotNullFromProvides(AppModule.INSTANCE.provideProductRemoteSource(apiInterface));
  }
}
