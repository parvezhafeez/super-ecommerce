package com.parvez.superecommerce.domain.usecase;

import com.parvez.superecommerce.data.repository.login.LoginRepository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LoginUseCase_Factory implements Factory<LoginUseCase> {
  private final Provider<LoginRepository> loginRepositoryProvider;

  public LoginUseCase_Factory(Provider<LoginRepository> loginRepositoryProvider) {
    this.loginRepositoryProvider = loginRepositoryProvider;
  }

  @Override
  public LoginUseCase get() {
    return newInstance(loginRepositoryProvider.get());
  }

  public static LoginUseCase_Factory create(Provider<LoginRepository> loginRepositoryProvider) {
    return new LoginUseCase_Factory(loginRepositoryProvider);
  }

  public static LoginUseCase newInstance(LoginRepository loginRepository) {
    return new LoginUseCase(loginRepository);
  }
}
