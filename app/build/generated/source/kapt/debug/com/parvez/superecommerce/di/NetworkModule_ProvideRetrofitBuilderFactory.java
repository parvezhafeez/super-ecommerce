package com.parvez.superecommerce.di;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import retrofit2.Retrofit;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NetworkModule_ProvideRetrofitBuilderFactory implements Factory<Retrofit.Builder> {
  @Override
  public Retrofit.Builder get() {
    return provideRetrofitBuilder();
  }

  public static NetworkModule_ProvideRetrofitBuilderFactory create() {
    return InstanceHolder.INSTANCE;
  }

  public static Retrofit.Builder provideRetrofitBuilder() {
    return Preconditions.checkNotNullFromProvides(NetworkModule.INSTANCE.provideRetrofitBuilder());
  }

  private static final class InstanceHolder {
    private static final NetworkModule_ProvideRetrofitBuilderFactory INSTANCE = new NetworkModule_ProvideRetrofitBuilderFactory();
  }
}
