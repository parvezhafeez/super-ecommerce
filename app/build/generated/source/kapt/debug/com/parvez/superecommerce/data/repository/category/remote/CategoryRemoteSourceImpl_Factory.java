package com.parvez.superecommerce.data.repository.category.remote;

import com.parvez.superecommerce.data.remote.ApiInterface;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CategoryRemoteSourceImpl_Factory implements Factory<CategoryRemoteSourceImpl> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public CategoryRemoteSourceImpl_Factory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public CategoryRemoteSourceImpl get() {
    return newInstance(apiInterfaceProvider.get());
  }

  public static CategoryRemoteSourceImpl_Factory create(
      Provider<ApiInterface> apiInterfaceProvider) {
    return new CategoryRemoteSourceImpl_Factory(apiInterfaceProvider);
  }

  public static CategoryRemoteSourceImpl newInstance(ApiInterface apiInterface) {
    return new CategoryRemoteSourceImpl(apiInterface);
  }
}
