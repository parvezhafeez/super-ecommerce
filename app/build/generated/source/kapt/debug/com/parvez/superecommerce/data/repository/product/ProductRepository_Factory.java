package com.parvez.superecommerce.data.repository.product;

import com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ProductRepository_Factory implements Factory<ProductRepository> {
  private final Provider<ProductRemoteSource> productRemoteSourceProvider;

  public ProductRepository_Factory(Provider<ProductRemoteSource> productRemoteSourceProvider) {
    this.productRemoteSourceProvider = productRemoteSourceProvider;
  }

  @Override
  public ProductRepository get() {
    return newInstance(productRemoteSourceProvider.get());
  }

  public static ProductRepository_Factory create(
      Provider<ProductRemoteSource> productRemoteSourceProvider) {
    return new ProductRepository_Factory(productRemoteSourceProvider);
  }

  public static ProductRepository newInstance(ProductRemoteSource productRemoteSource) {
    return new ProductRepository(productRemoteSource);
  }
}
