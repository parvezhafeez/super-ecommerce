package com.parvez.superecommerce.ui.splash;

import com.parvez.superecommerce.domain.usecase.GetStatusCheckUseCase;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class MainViewModel_Factory implements Factory<MainViewModel> {
  private final Provider<GetStatusCheckUseCase> getStatusCheckUseCaseProvider;

  public MainViewModel_Factory(Provider<GetStatusCheckUseCase> getStatusCheckUseCaseProvider) {
    this.getStatusCheckUseCaseProvider = getStatusCheckUseCaseProvider;
  }

  @Override
  public MainViewModel get() {
    return newInstance(getStatusCheckUseCaseProvider.get());
  }

  public static MainViewModel_Factory create(
      Provider<GetStatusCheckUseCase> getStatusCheckUseCaseProvider) {
    return new MainViewModel_Factory(getStatusCheckUseCaseProvider);
  }

  public static MainViewModel newInstance(GetStatusCheckUseCase getStatusCheckUseCase) {
    return new MainViewModel(getStatusCheckUseCase);
  }
}
