package com.parvez.superecommerce.data.repository.test.remote;

import com.parvez.superecommerce.data.remote.ApiInterface;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TestRemoteSourceImpl_Factory implements Factory<TestRemoteSourceImpl> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public TestRemoteSourceImpl_Factory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public TestRemoteSourceImpl get() {
    return newInstance(apiInterfaceProvider.get());
  }

  public static TestRemoteSourceImpl_Factory create(Provider<ApiInterface> apiInterfaceProvider) {
    return new TestRemoteSourceImpl_Factory(apiInterfaceProvider);
  }

  public static TestRemoteSourceImpl newInstance(ApiInterface apiInterface) {
    return new TestRemoteSourceImpl(apiInterface);
  }
}
