package com.parvez.superecommerce.di;

import com.parvez.superecommerce.data.remote.ApiInterface;
import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvideCartRemoteSourceFactory implements Factory<CartRemoteSource> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public AppModule_ProvideCartRemoteSourceFactory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public CartRemoteSource get() {
    return provideCartRemoteSource(apiInterfaceProvider.get());
  }

  public static AppModule_ProvideCartRemoteSourceFactory create(
      Provider<ApiInterface> apiInterfaceProvider) {
    return new AppModule_ProvideCartRemoteSourceFactory(apiInterfaceProvider);
  }

  public static CartRemoteSource provideCartRemoteSource(ApiInterface apiInterface) {
    return Preconditions.checkNotNullFromProvides(AppModule.INSTANCE.provideCartRemoteSource(apiInterface));
  }
}
