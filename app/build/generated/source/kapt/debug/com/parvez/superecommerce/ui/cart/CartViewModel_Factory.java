package com.parvez.superecommerce.ui.cart;

import com.parvez.superecommerce.domain.usecase.GetCartUseCase;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CartViewModel_Factory implements Factory<CartViewModel> {
  private final Provider<GetCartUseCase> getCartUseCaseProvider;

  public CartViewModel_Factory(Provider<GetCartUseCase> getCartUseCaseProvider) {
    this.getCartUseCaseProvider = getCartUseCaseProvider;
  }

  @Override
  public CartViewModel get() {
    return newInstance(getCartUseCaseProvider.get());
  }

  public static CartViewModel_Factory create(Provider<GetCartUseCase> getCartUseCaseProvider) {
    return new CartViewModel_Factory(getCartUseCaseProvider);
  }

  public static CartViewModel newInstance(GetCartUseCase getCartUseCase) {
    return new CartViewModel(getCartUseCase);
  }
}
