package com.parvez.superecommerce.data.repository.category;

import com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CategoryRepository_Factory implements Factory<CategoryRepository> {
  private final Provider<CategoryRemoteSource> categoryRemoteSourceProvider;

  public CategoryRepository_Factory(Provider<CategoryRemoteSource> categoryRemoteSourceProvider) {
    this.categoryRemoteSourceProvider = categoryRemoteSourceProvider;
  }

  @Override
  public CategoryRepository get() {
    return newInstance(categoryRemoteSourceProvider.get());
  }

  public static CategoryRepository_Factory create(
      Provider<CategoryRemoteSource> categoryRemoteSourceProvider) {
    return new CategoryRepository_Factory(categoryRemoteSourceProvider);
  }

  public static CategoryRepository newInstance(CategoryRemoteSource categoryRemoteSource) {
    return new CategoryRepository(categoryRemoteSource);
  }
}
