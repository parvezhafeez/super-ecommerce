package com.parvez.superecommerce.data.repository.post.remote;

import com.parvez.superecommerce.data.remote.ApiInterface;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PostRemoteSourceImpl_Factory implements Factory<PostRemoteSourceImpl> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public PostRemoteSourceImpl_Factory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public PostRemoteSourceImpl get() {
    return newInstance(apiInterfaceProvider.get());
  }

  public static PostRemoteSourceImpl_Factory create(Provider<ApiInterface> apiInterfaceProvider) {
    return new PostRemoteSourceImpl_Factory(apiInterfaceProvider);
  }

  public static PostRemoteSourceImpl newInstance(ApiInterface apiInterface) {
    return new PostRemoteSourceImpl(apiInterface);
  }
}
