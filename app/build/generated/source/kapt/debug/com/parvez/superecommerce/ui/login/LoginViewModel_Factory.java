package com.parvez.superecommerce.ui.login;

import com.parvez.superecommerce.data.prefdatastore.DataStoreManager;
import com.parvez.superecommerce.domain.usecase.LoginUseCase;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LoginViewModel_Factory implements Factory<LoginViewModel> {
  private final Provider<LoginUseCase> loginUseCaseProvider;

  private final Provider<DataStoreManager> dataStoreManagerProvider;

  public LoginViewModel_Factory(Provider<LoginUseCase> loginUseCaseProvider,
      Provider<DataStoreManager> dataStoreManagerProvider) {
    this.loginUseCaseProvider = loginUseCaseProvider;
    this.dataStoreManagerProvider = dataStoreManagerProvider;
  }

  @Override
  public LoginViewModel get() {
    return newInstance(loginUseCaseProvider.get(), dataStoreManagerProvider.get());
  }

  public static LoginViewModel_Factory create(Provider<LoginUseCase> loginUseCaseProvider,
      Provider<DataStoreManager> dataStoreManagerProvider) {
    return new LoginViewModel_Factory(loginUseCaseProvider, dataStoreManagerProvider);
  }

  public static LoginViewModel newInstance(LoginUseCase loginUseCase,
      DataStoreManager dataStoreManager) {
    return new LoginViewModel(loginUseCase, dataStoreManager);
  }
}
