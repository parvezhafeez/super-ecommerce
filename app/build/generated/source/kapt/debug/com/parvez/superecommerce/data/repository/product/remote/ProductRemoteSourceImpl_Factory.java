package com.parvez.superecommerce.data.repository.product.remote;

import com.parvez.superecommerce.data.remote.ApiInterface;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ProductRemoteSourceImpl_Factory implements Factory<ProductRemoteSourceImpl> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public ProductRemoteSourceImpl_Factory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public ProductRemoteSourceImpl get() {
    return newInstance(apiInterfaceProvider.get());
  }

  public static ProductRemoteSourceImpl_Factory create(
      Provider<ApiInterface> apiInterfaceProvider) {
    return new ProductRemoteSourceImpl_Factory(apiInterfaceProvider);
  }

  public static ProductRemoteSourceImpl newInstance(ApiInterface apiInterface) {
    return new ProductRemoteSourceImpl(apiInterface);
  }
}
