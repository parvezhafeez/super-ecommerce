package com.parvez.superecommerce.domain.usecase;

import com.parvez.superecommerce.data.repository.post.PostRepository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetPostUseCase_Factory implements Factory<GetPostUseCase> {
  private final Provider<PostRepository> postRepositoryProvider;

  public GetPostUseCase_Factory(Provider<PostRepository> postRepositoryProvider) {
    this.postRepositoryProvider = postRepositoryProvider;
  }

  @Override
  public GetPostUseCase get() {
    return newInstance(postRepositoryProvider.get());
  }

  public static GetPostUseCase_Factory create(Provider<PostRepository> postRepositoryProvider) {
    return new GetPostUseCase_Factory(postRepositoryProvider);
  }

  public static GetPostUseCase newInstance(PostRepository postRepository) {
    return new GetPostUseCase(postRepository);
  }
}
