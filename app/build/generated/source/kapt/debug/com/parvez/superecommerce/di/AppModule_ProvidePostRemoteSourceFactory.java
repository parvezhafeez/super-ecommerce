package com.parvez.superecommerce.di;

import com.parvez.superecommerce.data.remote.ApiInterface;
import com.parvez.superecommerce.data.repository.post.remote.PostRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvidePostRemoteSourceFactory implements Factory<PostRemoteSource> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public AppModule_ProvidePostRemoteSourceFactory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public PostRemoteSource get() {
    return providePostRemoteSource(apiInterfaceProvider.get());
  }

  public static AppModule_ProvidePostRemoteSourceFactory create(
      Provider<ApiInterface> apiInterfaceProvider) {
    return new AppModule_ProvidePostRemoteSourceFactory(apiInterfaceProvider);
  }

  public static PostRemoteSource providePostRemoteSource(ApiInterface apiInterface) {
    return Preconditions.checkNotNullFromProvides(AppModule.INSTANCE.providePostRemoteSource(apiInterface));
  }
}
