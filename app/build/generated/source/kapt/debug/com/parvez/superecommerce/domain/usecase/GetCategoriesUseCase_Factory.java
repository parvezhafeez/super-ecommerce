package com.parvez.superecommerce.domain.usecase;

import com.parvez.superecommerce.data.repository.category.CategoryRepository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetCategoriesUseCase_Factory implements Factory<GetCategoriesUseCase> {
  private final Provider<CategoryRepository> categoryRepositoryProvider;

  public GetCategoriesUseCase_Factory(Provider<CategoryRepository> categoryRepositoryProvider) {
    this.categoryRepositoryProvider = categoryRepositoryProvider;
  }

  @Override
  public GetCategoriesUseCase get() {
    return newInstance(categoryRepositoryProvider.get());
  }

  public static GetCategoriesUseCase_Factory create(
      Provider<CategoryRepository> categoryRepositoryProvider) {
    return new GetCategoriesUseCase_Factory(categoryRepositoryProvider);
  }

  public static GetCategoriesUseCase newInstance(CategoryRepository categoryRepository) {
    return new GetCategoriesUseCase(categoryRepository);
  }
}
