package com.parvez.superecommerce.ui.productdetails;

import androidx.lifecycle.SavedStateHandle;
import com.parvez.superecommerce.domain.usecase.AddProductToCartUseCase;
import com.parvez.superecommerce.domain.usecase.GetProductDetailUseCase;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ProductDetailViewModel_Factory implements Factory<ProductDetailViewModel> {
  private final Provider<SavedStateHandle> savedStateHandleProvider;

  private final Provider<AddProductToCartUseCase> addProductToCartUseCaseProvider;

  private final Provider<GetProductDetailUseCase> productDetailUseCaseProvider;

  public ProductDetailViewModel_Factory(Provider<SavedStateHandle> savedStateHandleProvider,
      Provider<AddProductToCartUseCase> addProductToCartUseCaseProvider,
      Provider<GetProductDetailUseCase> productDetailUseCaseProvider) {
    this.savedStateHandleProvider = savedStateHandleProvider;
    this.addProductToCartUseCaseProvider = addProductToCartUseCaseProvider;
    this.productDetailUseCaseProvider = productDetailUseCaseProvider;
  }

  @Override
  public ProductDetailViewModel get() {
    return newInstance(savedStateHandleProvider.get(), addProductToCartUseCaseProvider.get(), productDetailUseCaseProvider.get());
  }

  public static ProductDetailViewModel_Factory create(
      Provider<SavedStateHandle> savedStateHandleProvider,
      Provider<AddProductToCartUseCase> addProductToCartUseCaseProvider,
      Provider<GetProductDetailUseCase> productDetailUseCaseProvider) {
    return new ProductDetailViewModel_Factory(savedStateHandleProvider, addProductToCartUseCaseProvider, productDetailUseCaseProvider);
  }

  public static ProductDetailViewModel newInstance(SavedStateHandle savedStateHandle,
      AddProductToCartUseCase addProductToCartUseCase,
      GetProductDetailUseCase productDetailUseCase) {
    return new ProductDetailViewModel(savedStateHandle, addProductToCartUseCase, productDetailUseCase);
  }
}
