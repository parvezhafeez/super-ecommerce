package com.parvez.superecommerce.domain.usecase;

import com.parvez.superecommerce.data.repository.test.TestRepository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetStatusCheckUseCase_Factory implements Factory<GetStatusCheckUseCase> {
  private final Provider<TestRepository> testRepositoryProvider;

  public GetStatusCheckUseCase_Factory(Provider<TestRepository> testRepositoryProvider) {
    this.testRepositoryProvider = testRepositoryProvider;
  }

  @Override
  public GetStatusCheckUseCase get() {
    return newInstance(testRepositoryProvider.get());
  }

  public static GetStatusCheckUseCase_Factory create(
      Provider<TestRepository> testRepositoryProvider) {
    return new GetStatusCheckUseCase_Factory(testRepositoryProvider);
  }

  public static GetStatusCheckUseCase newInstance(TestRepository testRepository) {
    return new GetStatusCheckUseCase(testRepository);
  }
}
