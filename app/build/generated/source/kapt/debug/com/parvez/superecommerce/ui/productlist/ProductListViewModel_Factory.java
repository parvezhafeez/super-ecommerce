package com.parvez.superecommerce.ui.productlist;

import androidx.lifecycle.SavedStateHandle;
import com.parvez.superecommerce.domain.usecase.GetProductListUseCase;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ProductListViewModel_Factory implements Factory<ProductListViewModel> {
  private final Provider<SavedStateHandle> savedStateHandleProvider;

  private final Provider<GetProductListUseCase> productListUseCaseProvider;

  public ProductListViewModel_Factory(Provider<SavedStateHandle> savedStateHandleProvider,
      Provider<GetProductListUseCase> productListUseCaseProvider) {
    this.savedStateHandleProvider = savedStateHandleProvider;
    this.productListUseCaseProvider = productListUseCaseProvider;
  }

  @Override
  public ProductListViewModel get() {
    return newInstance(savedStateHandleProvider.get(), productListUseCaseProvider.get());
  }

  public static ProductListViewModel_Factory create(
      Provider<SavedStateHandle> savedStateHandleProvider,
      Provider<GetProductListUseCase> productListUseCaseProvider) {
    return new ProductListViewModel_Factory(savedStateHandleProvider, productListUseCaseProvider);
  }

  public static ProductListViewModel newInstance(SavedStateHandle savedStateHandle,
      GetProductListUseCase productListUseCase) {
    return new ProductListViewModel(savedStateHandle, productListUseCase);
  }
}
