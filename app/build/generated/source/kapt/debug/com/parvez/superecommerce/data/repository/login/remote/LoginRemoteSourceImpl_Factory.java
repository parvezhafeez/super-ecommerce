package com.parvez.superecommerce.data.repository.login.remote;

import com.parvez.superecommerce.data.remote.ApiInterface;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LoginRemoteSourceImpl_Factory implements Factory<LoginRemoteSourceImpl> {
  private final Provider<ApiInterface> apiInterfaceProvider;

  public LoginRemoteSourceImpl_Factory(Provider<ApiInterface> apiInterfaceProvider) {
    this.apiInterfaceProvider = apiInterfaceProvider;
  }

  @Override
  public LoginRemoteSourceImpl get() {
    return newInstance(apiInterfaceProvider.get());
  }

  public static LoginRemoteSourceImpl_Factory create(Provider<ApiInterface> apiInterfaceProvider) {
    return new LoginRemoteSourceImpl_Factory(apiInterfaceProvider);
  }

  public static LoginRemoteSourceImpl newInstance(ApiInterface apiInterface) {
    return new LoginRemoteSourceImpl(apiInterface);
  }
}
