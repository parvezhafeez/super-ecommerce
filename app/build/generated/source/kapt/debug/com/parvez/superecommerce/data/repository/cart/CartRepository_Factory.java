package com.parvez.superecommerce.data.repository.cart;

import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class CartRepository_Factory implements Factory<CartRepository> {
  private final Provider<CartRemoteSource> cartRemoteSourceProvider;

  public CartRepository_Factory(Provider<CartRemoteSource> cartRemoteSourceProvider) {
    this.cartRemoteSourceProvider = cartRemoteSourceProvider;
  }

  @Override
  public CartRepository get() {
    return newInstance(cartRemoteSourceProvider.get());
  }

  public static CartRepository_Factory create(Provider<CartRemoteSource> cartRemoteSourceProvider) {
    return new CartRepository_Factory(cartRemoteSourceProvider);
  }

  public static CartRepository newInstance(CartRemoteSource cartRemoteSource) {
    return new CartRepository(cartRemoteSource);
  }
}
