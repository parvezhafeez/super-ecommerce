package com.parvez.superecommerce.domain.usecase;

import com.parvez.superecommerce.data.repository.cart.CartRepository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.annotation.processing.Generated;
import javax.inject.Provider;

@ScopeMetadata
@QualifierMetadata
@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AddProductToCartUseCase_Factory implements Factory<AddProductToCartUseCase> {
  private final Provider<CartRepository> cartRepositoryProvider;

  public AddProductToCartUseCase_Factory(Provider<CartRepository> cartRepositoryProvider) {
    this.cartRepositoryProvider = cartRepositoryProvider;
  }

  @Override
  public AddProductToCartUseCase get() {
    return newInstance(cartRepositoryProvider.get());
  }

  public static AddProductToCartUseCase_Factory create(
      Provider<CartRepository> cartRepositoryProvider) {
    return new AddProductToCartUseCase_Factory(cartRepositoryProvider);
  }

  public static AddProductToCartUseCase newInstance(CartRepository cartRepository) {
    return new AddProductToCartUseCase(cartRepository);
  }
}
