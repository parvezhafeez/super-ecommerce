package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

@ProcessedRootSentinel(
    roots = "com.parvez.superecommerce.SuperEcommerceApplication"
)
public final class _com_parvez_superecommerce_SuperEcommerceApplication {
}
