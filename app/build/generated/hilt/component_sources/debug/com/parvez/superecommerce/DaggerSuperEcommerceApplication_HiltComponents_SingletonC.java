package com.parvez.superecommerce;

import android.app.Activity;
import android.app.Service;
import android.view.View;
import androidx.datastore.core.DataStore;
import androidx.datastore.preferences.core.Preferences;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import com.parvez.superecommerce.data.prefdatastore.DataStoreManager;
import com.parvez.superecommerce.data.remote.ApiInterface;
import com.parvez.superecommerce.data.repository.cart.CartRepository;
import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource;
import com.parvez.superecommerce.data.repository.category.CategoryRepository;
import com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSource;
import com.parvez.superecommerce.data.repository.login.LoginRepository;
import com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSource;
import com.parvez.superecommerce.data.repository.post.PostRepository;
import com.parvez.superecommerce.data.repository.post.remote.PostRemoteSource;
import com.parvez.superecommerce.data.repository.product.ProductRepository;
import com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource;
import com.parvez.superecommerce.data.repository.test.TestRepository;
import com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource;
import com.parvez.superecommerce.di.AppModule;
import com.parvez.superecommerce.di.AppModule_ProvideCartRemoteSourceFactory;
import com.parvez.superecommerce.di.AppModule_ProvideCategoryRemoteSourceFactory;
import com.parvez.superecommerce.di.AppModule_ProvideLoginRemoteSourceFactory;
import com.parvez.superecommerce.di.AppModule_ProvidePostRemoteSourceFactory;
import com.parvez.superecommerce.di.AppModule_ProvideProductRemoteSourceFactory;
import com.parvez.superecommerce.di.AppModule_ProvideTestRemoteSourceFactory;
import com.parvez.superecommerce.di.DataStoreModule;
import com.parvez.superecommerce.di.DataStoreModule_ProvideDataStoreFactory;
import com.parvez.superecommerce.di.NetworkModule;
import com.parvez.superecommerce.di.NetworkModule_ProvideApiInterfaceFactory;
import com.parvez.superecommerce.di.NetworkModule_ProvideHttpLoggingInterceptorFactory;
import com.parvez.superecommerce.di.NetworkModule_ProvideOkHttpClientFactory;
import com.parvez.superecommerce.di.NetworkModule_ProvideRetrofitBuilderFactory;
import com.parvez.superecommerce.domain.usecase.AddProductToCartUseCase;
import com.parvez.superecommerce.domain.usecase.GetCartUseCase;
import com.parvez.superecommerce.domain.usecase.GetCategoriesUseCase;
import com.parvez.superecommerce.domain.usecase.GetPostUseCase;
import com.parvez.superecommerce.domain.usecase.GetProductDetailUseCase;
import com.parvez.superecommerce.domain.usecase.GetProductListUseCase;
import com.parvez.superecommerce.domain.usecase.GetStatusCheckUseCase;
import com.parvez.superecommerce.domain.usecase.LoginUseCase;
import com.parvez.superecommerce.ui.MainActivity;
import com.parvez.superecommerce.ui.cart.CartViewModel;
import com.parvez.superecommerce.ui.cart.CartViewModel_HiltModules_KeyModule_ProvideFactory;
import com.parvez.superecommerce.ui.home.HomeViewModel;
import com.parvez.superecommerce.ui.home.HomeViewModel_HiltModules_KeyModule_ProvideFactory;
import com.parvez.superecommerce.ui.login.LoginViewModel;
import com.parvez.superecommerce.ui.login.LoginViewModel_HiltModules_KeyModule_ProvideFactory;
import com.parvez.superecommerce.ui.post.ProductViewModel;
import com.parvez.superecommerce.ui.post.ProductViewModel_HiltModules_KeyModule_ProvideFactory;
import com.parvez.superecommerce.ui.productdetails.ProductDetailViewModel;
import com.parvez.superecommerce.ui.productdetails.ProductDetailViewModel_HiltModules_KeyModule_ProvideFactory;
import com.parvez.superecommerce.ui.productlist.ProductListViewModel;
import com.parvez.superecommerce.ui.productlist.ProductListViewModel_HiltModules_KeyModule_ProvideFactory;
import com.parvez.superecommerce.ui.splash.MainViewModel;
import com.parvez.superecommerce.ui.splash.MainViewModel_HiltModules_KeyModule_ProvideFactory;
import dagger.hilt.android.ActivityRetainedLifecycle;
import dagger.hilt.android.ViewModelLifecycle;
import dagger.hilt.android.flags.HiltWrapper_FragmentGetContextFix_FragmentGetContextFixModule;
import dagger.hilt.android.internal.builders.ActivityComponentBuilder;
import dagger.hilt.android.internal.builders.ActivityRetainedComponentBuilder;
import dagger.hilt.android.internal.builders.FragmentComponentBuilder;
import dagger.hilt.android.internal.builders.ServiceComponentBuilder;
import dagger.hilt.android.internal.builders.ViewComponentBuilder;
import dagger.hilt.android.internal.builders.ViewModelComponentBuilder;
import dagger.hilt.android.internal.builders.ViewWithFragmentComponentBuilder;
import dagger.hilt.android.internal.lifecycle.DefaultViewModelFactories;
import dagger.hilt.android.internal.lifecycle.DefaultViewModelFactories_InternalFactoryFactory_Factory;
import dagger.hilt.android.internal.managers.ActivityRetainedComponentManager_LifecycleModule_ProvideActivityRetainedLifecycleFactory;
import dagger.hilt.android.internal.modules.ApplicationContextModule;
import dagger.hilt.android.internal.modules.ApplicationContextModule_ProvideContextFactory;
import dagger.internal.DaggerGenerated;
import dagger.internal.DoubleCheck;
import dagger.internal.MapBuilder;
import dagger.internal.Preconditions;
import dagger.internal.SetBuilder;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@DaggerGenerated
@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DaggerSuperEcommerceApplication_HiltComponents_SingletonC {
  private DaggerSuperEcommerceApplication_HiltComponents_SingletonC() {
  }

  public static Builder builder() {
    return new Builder();
  }

  public static final class Builder {
    private ApplicationContextModule applicationContextModule;

    private Builder() {
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://dagger.dev/unused-modules.
     */
    @Deprecated
    public Builder appModule(AppModule appModule) {
      Preconditions.checkNotNull(appModule);
      return this;
    }

    public Builder applicationContextModule(ApplicationContextModule applicationContextModule) {
      this.applicationContextModule = Preconditions.checkNotNull(applicationContextModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://dagger.dev/unused-modules.
     */
    @Deprecated
    public Builder dataStoreModule(DataStoreModule dataStoreModule) {
      Preconditions.checkNotNull(dataStoreModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://dagger.dev/unused-modules.
     */
    @Deprecated
    public Builder hiltWrapper_FragmentGetContextFix_FragmentGetContextFixModule(
        HiltWrapper_FragmentGetContextFix_FragmentGetContextFixModule hiltWrapper_FragmentGetContextFix_FragmentGetContextFixModule) {
      Preconditions.checkNotNull(hiltWrapper_FragmentGetContextFix_FragmentGetContextFixModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://dagger.dev/unused-modules.
     */
    @Deprecated
    public Builder networkModule(NetworkModule networkModule) {
      Preconditions.checkNotNull(networkModule);
      return this;
    }

    public SuperEcommerceApplication_HiltComponents.SingletonC build() {
      Preconditions.checkBuilderRequirement(applicationContextModule, ApplicationContextModule.class);
      return new SingletonCImpl(applicationContextModule);
    }
  }

  private static final class ActivityRetainedCBuilder implements SuperEcommerceApplication_HiltComponents.ActivityRetainedC.Builder {
    private final SingletonCImpl singletonCImpl;

    private ActivityRetainedCBuilder(SingletonCImpl singletonCImpl) {
      this.singletonCImpl = singletonCImpl;
    }

    @Override
    public SuperEcommerceApplication_HiltComponents.ActivityRetainedC build() {
      return new ActivityRetainedCImpl(singletonCImpl);
    }
  }

  private static final class ActivityCBuilder implements SuperEcommerceApplication_HiltComponents.ActivityC.Builder {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private Activity activity;

    private ActivityCBuilder(SingletonCImpl singletonCImpl,
        ActivityRetainedCImpl activityRetainedCImpl) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
    }

    @Override
    public ActivityCBuilder activity(Activity activity) {
      this.activity = Preconditions.checkNotNull(activity);
      return this;
    }

    @Override
    public SuperEcommerceApplication_HiltComponents.ActivityC build() {
      Preconditions.checkBuilderRequirement(activity, Activity.class);
      return new ActivityCImpl(singletonCImpl, activityRetainedCImpl, activity);
    }
  }

  private static final class FragmentCBuilder implements SuperEcommerceApplication_HiltComponents.FragmentC.Builder {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private Fragment fragment;

    private FragmentCBuilder(SingletonCImpl singletonCImpl,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;
    }

    @Override
    public FragmentCBuilder fragment(Fragment fragment) {
      this.fragment = Preconditions.checkNotNull(fragment);
      return this;
    }

    @Override
    public SuperEcommerceApplication_HiltComponents.FragmentC build() {
      Preconditions.checkBuilderRequirement(fragment, Fragment.class);
      return new FragmentCImpl(singletonCImpl, activityRetainedCImpl, activityCImpl, fragment);
    }
  }

  private static final class ViewWithFragmentCBuilder implements SuperEcommerceApplication_HiltComponents.ViewWithFragmentC.Builder {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private final FragmentCImpl fragmentCImpl;

    private View view;

    private ViewWithFragmentCBuilder(SingletonCImpl singletonCImpl,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl,
        FragmentCImpl fragmentCImpl) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;
      this.fragmentCImpl = fragmentCImpl;
    }

    @Override
    public ViewWithFragmentCBuilder view(View view) {
      this.view = Preconditions.checkNotNull(view);
      return this;
    }

    @Override
    public SuperEcommerceApplication_HiltComponents.ViewWithFragmentC build() {
      Preconditions.checkBuilderRequirement(view, View.class);
      return new ViewWithFragmentCImpl(singletonCImpl, activityRetainedCImpl, activityCImpl, fragmentCImpl, view);
    }
  }

  private static final class ViewCBuilder implements SuperEcommerceApplication_HiltComponents.ViewC.Builder {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private View view;

    private ViewCBuilder(SingletonCImpl singletonCImpl, ActivityRetainedCImpl activityRetainedCImpl,
        ActivityCImpl activityCImpl) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;
    }

    @Override
    public ViewCBuilder view(View view) {
      this.view = Preconditions.checkNotNull(view);
      return this;
    }

    @Override
    public SuperEcommerceApplication_HiltComponents.ViewC build() {
      Preconditions.checkBuilderRequirement(view, View.class);
      return new ViewCImpl(singletonCImpl, activityRetainedCImpl, activityCImpl, view);
    }
  }

  private static final class ViewModelCBuilder implements SuperEcommerceApplication_HiltComponents.ViewModelC.Builder {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private SavedStateHandle savedStateHandle;

    private ViewModelLifecycle viewModelLifecycle;

    private ViewModelCBuilder(SingletonCImpl singletonCImpl,
        ActivityRetainedCImpl activityRetainedCImpl) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
    }

    @Override
    public ViewModelCBuilder savedStateHandle(SavedStateHandle handle) {
      this.savedStateHandle = Preconditions.checkNotNull(handle);
      return this;
    }

    @Override
    public ViewModelCBuilder viewModelLifecycle(ViewModelLifecycle viewModelLifecycle) {
      this.viewModelLifecycle = Preconditions.checkNotNull(viewModelLifecycle);
      return this;
    }

    @Override
    public SuperEcommerceApplication_HiltComponents.ViewModelC build() {
      Preconditions.checkBuilderRequirement(savedStateHandle, SavedStateHandle.class);
      Preconditions.checkBuilderRequirement(viewModelLifecycle, ViewModelLifecycle.class);
      return new ViewModelCImpl(singletonCImpl, activityRetainedCImpl, savedStateHandle, viewModelLifecycle);
    }
  }

  private static final class ServiceCBuilder implements SuperEcommerceApplication_HiltComponents.ServiceC.Builder {
    private final SingletonCImpl singletonCImpl;

    private Service service;

    private ServiceCBuilder(SingletonCImpl singletonCImpl) {
      this.singletonCImpl = singletonCImpl;
    }

    @Override
    public ServiceCBuilder service(Service service) {
      this.service = Preconditions.checkNotNull(service);
      return this;
    }

    @Override
    public SuperEcommerceApplication_HiltComponents.ServiceC build() {
      Preconditions.checkBuilderRequirement(service, Service.class);
      return new ServiceCImpl(singletonCImpl, service);
    }
  }

  private static final class ViewWithFragmentCImpl extends SuperEcommerceApplication_HiltComponents.ViewWithFragmentC {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private final FragmentCImpl fragmentCImpl;

    private final ViewWithFragmentCImpl viewWithFragmentCImpl = this;

    private ViewWithFragmentCImpl(SingletonCImpl singletonCImpl,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl,
        FragmentCImpl fragmentCImpl, View viewParam) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;
      this.fragmentCImpl = fragmentCImpl;


    }
  }

  private static final class FragmentCImpl extends SuperEcommerceApplication_HiltComponents.FragmentC {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private final FragmentCImpl fragmentCImpl = this;

    private FragmentCImpl(SingletonCImpl singletonCImpl,
        ActivityRetainedCImpl activityRetainedCImpl, ActivityCImpl activityCImpl,
        Fragment fragmentParam) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;


    }

    @Override
    public DefaultViewModelFactories.InternalFactoryFactory getHiltInternalFactoryFactory() {
      return activityCImpl.getHiltInternalFactoryFactory();
    }

    @Override
    public ViewWithFragmentComponentBuilder viewWithFragmentComponentBuilder() {
      return new ViewWithFragmentCBuilder(singletonCImpl, activityRetainedCImpl, activityCImpl, fragmentCImpl);
    }
  }

  private static final class ViewCImpl extends SuperEcommerceApplication_HiltComponents.ViewC {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl;

    private final ViewCImpl viewCImpl = this;

    private ViewCImpl(SingletonCImpl singletonCImpl, ActivityRetainedCImpl activityRetainedCImpl,
        ActivityCImpl activityCImpl, View viewParam) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.activityCImpl = activityCImpl;


    }
  }

  private static final class ActivityCImpl extends SuperEcommerceApplication_HiltComponents.ActivityC {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ActivityCImpl activityCImpl = this;

    private ActivityCImpl(SingletonCImpl singletonCImpl,
        ActivityRetainedCImpl activityRetainedCImpl, Activity activityParam) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;


    }

    @Override
    public void injectMainActivity(MainActivity arg0) {
    }

    @Override
    public DefaultViewModelFactories.InternalFactoryFactory getHiltInternalFactoryFactory() {
      return DefaultViewModelFactories_InternalFactoryFactory_Factory.newInstance(getViewModelKeys(), new ViewModelCBuilder(singletonCImpl, activityRetainedCImpl));
    }

    @Override
    public Set<String> getViewModelKeys() {
      return SetBuilder.<String>newSetBuilder(7).add(CartViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(HomeViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(LoginViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(MainViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(ProductDetailViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(ProductListViewModel_HiltModules_KeyModule_ProvideFactory.provide()).add(ProductViewModel_HiltModules_KeyModule_ProvideFactory.provide()).build();
    }

    @Override
    public ViewModelComponentBuilder getViewModelComponentBuilder() {
      return new ViewModelCBuilder(singletonCImpl, activityRetainedCImpl);
    }

    @Override
    public FragmentComponentBuilder fragmentComponentBuilder() {
      return new FragmentCBuilder(singletonCImpl, activityRetainedCImpl, activityCImpl);
    }

    @Override
    public ViewComponentBuilder viewComponentBuilder() {
      return new ViewCBuilder(singletonCImpl, activityRetainedCImpl, activityCImpl);
    }
  }

  private static final class ViewModelCImpl extends SuperEcommerceApplication_HiltComponents.ViewModelC {
    private final SavedStateHandle savedStateHandle;

    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl;

    private final ViewModelCImpl viewModelCImpl = this;

    private Provider<CartViewModel> cartViewModelProvider;

    private Provider<HomeViewModel> homeViewModelProvider;

    private Provider<LoginViewModel> loginViewModelProvider;

    private Provider<MainViewModel> mainViewModelProvider;

    private Provider<ProductDetailViewModel> productDetailViewModelProvider;

    private Provider<ProductListViewModel> productListViewModelProvider;

    private Provider<ProductViewModel> productViewModelProvider;

    private ViewModelCImpl(SingletonCImpl singletonCImpl,
        ActivityRetainedCImpl activityRetainedCImpl, SavedStateHandle savedStateHandleParam,
        ViewModelLifecycle viewModelLifecycleParam) {
      this.singletonCImpl = singletonCImpl;
      this.activityRetainedCImpl = activityRetainedCImpl;
      this.savedStateHandle = savedStateHandleParam;
      initialize(savedStateHandleParam, viewModelLifecycleParam);

    }

    private CartRepository cartRepository() {
      return new CartRepository(singletonCImpl.provideCartRemoteSourceProvider.get());
    }

    private GetCartUseCase getCartUseCase() {
      return new GetCartUseCase(cartRepository());
    }

    private CategoryRepository categoryRepository() {
      return new CategoryRepository(singletonCImpl.provideCategoryRemoteSourceProvider.get());
    }

    private GetCategoriesUseCase getCategoriesUseCase() {
      return new GetCategoriesUseCase(categoryRepository());
    }

    private LoginRepository loginRepository() {
      return new LoginRepository(singletonCImpl.provideLoginRemoteSourceProvider.get());
    }

    private LoginUseCase loginUseCase() {
      return new LoginUseCase(loginRepository());
    }

    private DataStoreManager dataStoreManager() {
      return new DataStoreManager(singletonCImpl.provideDataStoreProvider.get());
    }

    private TestRepository testRepository() {
      return new TestRepository(singletonCImpl.provideTestRemoteSourceProvider.get());
    }

    private GetStatusCheckUseCase getStatusCheckUseCase() {
      return new GetStatusCheckUseCase(testRepository());
    }

    private AddProductToCartUseCase addProductToCartUseCase() {
      return new AddProductToCartUseCase(cartRepository());
    }

    private ProductRepository productRepository() {
      return new ProductRepository(singletonCImpl.provideProductRemoteSourceProvider.get());
    }

    private GetProductDetailUseCase getProductDetailUseCase() {
      return new GetProductDetailUseCase(productRepository());
    }

    private GetProductListUseCase getProductListUseCase() {
      return new GetProductListUseCase(productRepository());
    }

    private PostRepository postRepository() {
      return new PostRepository(singletonCImpl.providePostRemoteSourceProvider.get());
    }

    private GetPostUseCase getPostUseCase() {
      return new GetPostUseCase(postRepository());
    }

    @SuppressWarnings("unchecked")
    private void initialize(final SavedStateHandle savedStateHandleParam,
        final ViewModelLifecycle viewModelLifecycleParam) {
      this.cartViewModelProvider = new SwitchingProvider<>(singletonCImpl, activityRetainedCImpl, viewModelCImpl, 0);
      this.homeViewModelProvider = new SwitchingProvider<>(singletonCImpl, activityRetainedCImpl, viewModelCImpl, 1);
      this.loginViewModelProvider = new SwitchingProvider<>(singletonCImpl, activityRetainedCImpl, viewModelCImpl, 2);
      this.mainViewModelProvider = new SwitchingProvider<>(singletonCImpl, activityRetainedCImpl, viewModelCImpl, 3);
      this.productDetailViewModelProvider = new SwitchingProvider<>(singletonCImpl, activityRetainedCImpl, viewModelCImpl, 4);
      this.productListViewModelProvider = new SwitchingProvider<>(singletonCImpl, activityRetainedCImpl, viewModelCImpl, 5);
      this.productViewModelProvider = new SwitchingProvider<>(singletonCImpl, activityRetainedCImpl, viewModelCImpl, 6);
    }

    @Override
    public Map<String, Provider<ViewModel>> getHiltViewModelMap() {
      return MapBuilder.<String, Provider<ViewModel>>newMapBuilder(7).put("com.parvez.superecommerce.ui.cart.CartViewModel", ((Provider) cartViewModelProvider)).put("com.parvez.superecommerce.ui.home.HomeViewModel", ((Provider) homeViewModelProvider)).put("com.parvez.superecommerce.ui.login.LoginViewModel", ((Provider) loginViewModelProvider)).put("com.parvez.superecommerce.ui.splash.MainViewModel", ((Provider) mainViewModelProvider)).put("com.parvez.superecommerce.ui.productdetails.ProductDetailViewModel", ((Provider) productDetailViewModelProvider)).put("com.parvez.superecommerce.ui.productlist.ProductListViewModel", ((Provider) productListViewModelProvider)).put("com.parvez.superecommerce.ui.post.ProductViewModel", ((Provider) productViewModelProvider)).build();
    }

    private static final class SwitchingProvider<T> implements Provider<T> {
      private final SingletonCImpl singletonCImpl;

      private final ActivityRetainedCImpl activityRetainedCImpl;

      private final ViewModelCImpl viewModelCImpl;

      private final int id;

      SwitchingProvider(SingletonCImpl singletonCImpl, ActivityRetainedCImpl activityRetainedCImpl,
          ViewModelCImpl viewModelCImpl, int id) {
        this.singletonCImpl = singletonCImpl;
        this.activityRetainedCImpl = activityRetainedCImpl;
        this.viewModelCImpl = viewModelCImpl;
        this.id = id;
      }

      @SuppressWarnings("unchecked")
      @Override
      public T get() {
        switch (id) {
          case 0: // com.parvez.superecommerce.ui.cart.CartViewModel 
          return (T) new CartViewModel(viewModelCImpl.getCartUseCase());

          case 1: // com.parvez.superecommerce.ui.home.HomeViewModel 
          return (T) new HomeViewModel(viewModelCImpl.getCategoriesUseCase());

          case 2: // com.parvez.superecommerce.ui.login.LoginViewModel 
          return (T) new LoginViewModel(viewModelCImpl.loginUseCase(), viewModelCImpl.dataStoreManager());

          case 3: // com.parvez.superecommerce.ui.splash.MainViewModel 
          return (T) new MainViewModel(viewModelCImpl.getStatusCheckUseCase());

          case 4: // com.parvez.superecommerce.ui.productdetails.ProductDetailViewModel 
          return (T) new ProductDetailViewModel(viewModelCImpl.savedStateHandle, viewModelCImpl.addProductToCartUseCase(), viewModelCImpl.getProductDetailUseCase());

          case 5: // com.parvez.superecommerce.ui.productlist.ProductListViewModel 
          return (T) new ProductListViewModel(viewModelCImpl.savedStateHandle, viewModelCImpl.getProductListUseCase());

          case 6: // com.parvez.superecommerce.ui.post.ProductViewModel 
          return (T) new ProductViewModel(viewModelCImpl.savedStateHandle, viewModelCImpl.getPostUseCase());

          default: throw new AssertionError(id);
        }
      }
    }
  }

  private static final class ActivityRetainedCImpl extends SuperEcommerceApplication_HiltComponents.ActivityRetainedC {
    private final SingletonCImpl singletonCImpl;

    private final ActivityRetainedCImpl activityRetainedCImpl = this;

    private Provider<ActivityRetainedLifecycle> provideActivityRetainedLifecycleProvider;

    private ActivityRetainedCImpl(SingletonCImpl singletonCImpl) {
      this.singletonCImpl = singletonCImpl;

      initialize();

    }

    @SuppressWarnings("unchecked")
    private void initialize() {
      this.provideActivityRetainedLifecycleProvider = DoubleCheck.provider(new SwitchingProvider<ActivityRetainedLifecycle>(singletonCImpl, activityRetainedCImpl, 0));
    }

    @Override
    public ActivityComponentBuilder activityComponentBuilder() {
      return new ActivityCBuilder(singletonCImpl, activityRetainedCImpl);
    }

    @Override
    public ActivityRetainedLifecycle getActivityRetainedLifecycle() {
      return provideActivityRetainedLifecycleProvider.get();
    }

    private static final class SwitchingProvider<T> implements Provider<T> {
      private final SingletonCImpl singletonCImpl;

      private final ActivityRetainedCImpl activityRetainedCImpl;

      private final int id;

      SwitchingProvider(SingletonCImpl singletonCImpl, ActivityRetainedCImpl activityRetainedCImpl,
          int id) {
        this.singletonCImpl = singletonCImpl;
        this.activityRetainedCImpl = activityRetainedCImpl;
        this.id = id;
      }

      @SuppressWarnings("unchecked")
      @Override
      public T get() {
        switch (id) {
          case 0: // dagger.hilt.android.ActivityRetainedLifecycle 
          return (T) ActivityRetainedComponentManager_LifecycleModule_ProvideActivityRetainedLifecycleFactory.provideActivityRetainedLifecycle();

          default: throw new AssertionError(id);
        }
      }
    }
  }

  private static final class ServiceCImpl extends SuperEcommerceApplication_HiltComponents.ServiceC {
    private final SingletonCImpl singletonCImpl;

    private final ServiceCImpl serviceCImpl = this;

    private ServiceCImpl(SingletonCImpl singletonCImpl, Service serviceParam) {
      this.singletonCImpl = singletonCImpl;


    }
  }

  private static final class SingletonCImpl extends SuperEcommerceApplication_HiltComponents.SingletonC {
    private final ApplicationContextModule applicationContextModule;

    private final SingletonCImpl singletonCImpl = this;

    private Provider<retrofit2.Retrofit.Builder> provideRetrofitBuilderProvider;

    private Provider<HttpLoggingInterceptor> provideHttpLoggingInterceptorProvider;

    private Provider<OkHttpClient> provideOkHttpClientProvider;

    private Provider<ApiInterface> provideApiInterfaceProvider;

    private Provider<CartRemoteSource> provideCartRemoteSourceProvider;

    private Provider<CategoryRemoteSource> provideCategoryRemoteSourceProvider;

    private Provider<LoginRemoteSource> provideLoginRemoteSourceProvider;

    private Provider<DataStore<Preferences>> provideDataStoreProvider;

    private Provider<TestRemoteSource> provideTestRemoteSourceProvider;

    private Provider<ProductRemoteSource> provideProductRemoteSourceProvider;

    private Provider<PostRemoteSource> providePostRemoteSourceProvider;

    private SingletonCImpl(ApplicationContextModule applicationContextModuleParam) {
      this.applicationContextModule = applicationContextModuleParam;
      initialize(applicationContextModuleParam);

    }

    @SuppressWarnings("unchecked")
    private void initialize(final ApplicationContextModule applicationContextModuleParam) {
      this.provideRetrofitBuilderProvider = DoubleCheck.provider(new SwitchingProvider<retrofit2.Retrofit.Builder>(singletonCImpl, 2));
      this.provideHttpLoggingInterceptorProvider = DoubleCheck.provider(new SwitchingProvider<HttpLoggingInterceptor>(singletonCImpl, 4));
      this.provideOkHttpClientProvider = DoubleCheck.provider(new SwitchingProvider<OkHttpClient>(singletonCImpl, 3));
      this.provideApiInterfaceProvider = DoubleCheck.provider(new SwitchingProvider<ApiInterface>(singletonCImpl, 1));
      this.provideCartRemoteSourceProvider = DoubleCheck.provider(new SwitchingProvider<CartRemoteSource>(singletonCImpl, 0));
      this.provideCategoryRemoteSourceProvider = DoubleCheck.provider(new SwitchingProvider<CategoryRemoteSource>(singletonCImpl, 5));
      this.provideLoginRemoteSourceProvider = DoubleCheck.provider(new SwitchingProvider<LoginRemoteSource>(singletonCImpl, 6));
      this.provideDataStoreProvider = DoubleCheck.provider(new SwitchingProvider<DataStore<Preferences>>(singletonCImpl, 7));
      this.provideTestRemoteSourceProvider = DoubleCheck.provider(new SwitchingProvider<TestRemoteSource>(singletonCImpl, 8));
      this.provideProductRemoteSourceProvider = DoubleCheck.provider(new SwitchingProvider<ProductRemoteSource>(singletonCImpl, 9));
      this.providePostRemoteSourceProvider = DoubleCheck.provider(new SwitchingProvider<PostRemoteSource>(singletonCImpl, 10));
    }

    @Override
    public void injectSuperEcommerceApplication(SuperEcommerceApplication arg0) {
    }

    @Override
    public Set<Boolean> getDisableFragmentGetContextFix() {
      return Collections.<Boolean>emptySet();
    }

    @Override
    public ActivityRetainedComponentBuilder retainedComponentBuilder() {
      return new ActivityRetainedCBuilder(singletonCImpl);
    }

    @Override
    public ServiceComponentBuilder serviceComponentBuilder() {
      return new ServiceCBuilder(singletonCImpl);
    }

    private static final class SwitchingProvider<T> implements Provider<T> {
      private final SingletonCImpl singletonCImpl;

      private final int id;

      SwitchingProvider(SingletonCImpl singletonCImpl, int id) {
        this.singletonCImpl = singletonCImpl;
        this.id = id;
      }

      @SuppressWarnings("unchecked")
      @Override
      public T get() {
        switch (id) {
          case 0: // com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource 
          return (T) AppModule_ProvideCartRemoteSourceFactory.provideCartRemoteSource(singletonCImpl.provideApiInterfaceProvider.get());

          case 1: // com.parvez.superecommerce.data.remote.ApiInterface 
          return (T) NetworkModule_ProvideApiInterfaceFactory.provideApiInterface(singletonCImpl.provideRetrofitBuilderProvider.get(), singletonCImpl.provideOkHttpClientProvider.get());

          case 2: // retrofit2.Retrofit.Builder 
          return (T) NetworkModule_ProvideRetrofitBuilderFactory.provideRetrofitBuilder();

          case 3: // okhttp3.OkHttpClient 
          return (T) NetworkModule_ProvideOkHttpClientFactory.provideOkHttpClient(singletonCImpl.provideHttpLoggingInterceptorProvider.get());

          case 4: // okhttp3.logging.HttpLoggingInterceptor 
          return (T) NetworkModule_ProvideHttpLoggingInterceptorFactory.provideHttpLoggingInterceptor();

          case 5: // com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSource 
          return (T) AppModule_ProvideCategoryRemoteSourceFactory.provideCategoryRemoteSource(singletonCImpl.provideApiInterfaceProvider.get());

          case 6: // com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSource 
          return (T) AppModule_ProvideLoginRemoteSourceFactory.provideLoginRemoteSource(singletonCImpl.provideApiInterfaceProvider.get());

          case 7: // androidx.datastore.core.DataStore<androidx.datastore.preferences.core.Preferences> 
          return (T) DataStoreModule_ProvideDataStoreFactory.provideDataStore(ApplicationContextModule_ProvideContextFactory.provideContext(singletonCImpl.applicationContextModule));

          case 8: // com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource 
          return (T) AppModule_ProvideTestRemoteSourceFactory.provideTestRemoteSource(singletonCImpl.provideApiInterfaceProvider.get());

          case 9: // com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource 
          return (T) AppModule_ProvideProductRemoteSourceFactory.provideProductRemoteSource(singletonCImpl.provideApiInterfaceProvider.get());

          case 10: // com.parvez.superecommerce.data.repository.post.remote.PostRemoteSource 
          return (T) AppModule_ProvidePostRemoteSourceFactory.providePostRemoteSource(singletonCImpl.provideApiInterfaceProvider.get());

          default: throw new AssertionError(id);
        }
      }
    }
  }
}
