package com.parvez.superecommerce.ui.home;

import androidx.compose.runtime.State;
import androidx.lifecycle.ViewModel;
import com.parvez.superecommerce.data.model.category.CategoryResponse;
import com.parvez.superecommerce.data.remote.NetworkResponse;
import com.parvez.superecommerce.domain.usecase.GetCategoriesUseCase;
import dagger.hilt.android.lifecycle.HiltViewModel;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\f\u001a\u00020\rH\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\t8F\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/parvez/superecommerce/ui/home/HomeViewModel;", "Landroidx/lifecycle/ViewModel;", "getCategoriesUseCase", "Lcom/parvez/superecommerce/domain/usecase/GetCategoriesUseCase;", "(Lcom/parvez/superecommerce/domain/usecase/GetCategoriesUseCase;)V", "_state", "Landroidx/compose/runtime/MutableState;", "Lcom/parvez/superecommerce/ui/home/HomeUiState;", "state", "Landroidx/compose/runtime/State;", "getState", "()Landroidx/compose/runtime/State;", "getCategories", "", "app_debug"})
public final class HomeViewModel extends androidx.lifecycle.ViewModel {
    private final com.parvez.superecommerce.domain.usecase.GetCategoriesUseCase getCategoriesUseCase = null;
    private final androidx.compose.runtime.MutableState<com.parvez.superecommerce.ui.home.HomeUiState> _state = null;
    
    @javax.inject.Inject
    public HomeViewModel(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.domain.usecase.GetCategoriesUseCase getCategoriesUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.compose.runtime.State<com.parvez.superecommerce.ui.home.HomeUiState> getState() {
        return null;
    }
    
    private final void getCategories() {
    }
}