package com.parvez.superecommerce.ui.login;

import androidx.compose.runtime.State;
import androidx.lifecycle.ViewModel;
import com.parvez.superecommerce.data.model.login.LoginResponse;
import com.parvez.superecommerce.data.prefdatastore.DataStoreManager;
import com.parvez.superecommerce.data.remote.NetworkResponse;
import com.parvez.superecommerce.domain.usecase.LoginUseCase;
import dagger.hilt.android.lifecycle.HiltViewModel;
import kotlinx.coroutines.Deferred;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0011\u0010\u000e\u001a\u00020\u000fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J\u0016\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u000fJ\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u000fH\u0002R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u000b8F\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\r\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0017"}, d2 = {"Lcom/parvez/superecommerce/ui/login/LoginViewModel;", "Landroidx/lifecycle/ViewModel;", "loginUseCase", "Lcom/parvez/superecommerce/domain/usecase/LoginUseCase;", "dataStoreManager", "Lcom/parvez/superecommerce/data/prefdatastore/DataStoreManager;", "(Lcom/parvez/superecommerce/domain/usecase/LoginUseCase;Lcom/parvez/superecommerce/data/prefdatastore/DataStoreManager;)V", "_state", "Landroidx/compose/runtime/MutableState;", "Lcom/parvez/superecommerce/ui/login/LoginUiState;", "state", "Landroidx/compose/runtime/State;", "getState", "()Landroidx/compose/runtime/State;", "getUserToken", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "login", "", "username", "password", "saveUserToken", "userToken", "app_debug"})
public final class LoginViewModel extends androidx.lifecycle.ViewModel {
    private final com.parvez.superecommerce.domain.usecase.LoginUseCase loginUseCase = null;
    private final com.parvez.superecommerce.data.prefdatastore.DataStoreManager dataStoreManager = null;
    private final androidx.compose.runtime.MutableState<com.parvez.superecommerce.ui.login.LoginUiState> _state = null;
    
    @javax.inject.Inject
    public LoginViewModel(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.domain.usecase.LoginUseCase loginUseCase, @org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.prefdatastore.DataStoreManager dataStoreManager) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.compose.runtime.State<com.parvez.superecommerce.ui.login.LoginUiState> getState() {
        return null;
    }
    
    public final void login(@org.jetbrains.annotations.NotNull
    java.lang.String username, @org.jetbrains.annotations.NotNull
    java.lang.String password) {
    }
    
    private final void saveUserToken(java.lang.String userToken) {
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Object getUserToken(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super java.lang.String> continuation) {
        return null;
    }
}