package com.parvez.superecommerce.data.repository.test.remote;

import com.parvez.superecommerce.data.model.statuscheck.StatusCheck;
import com.parvez.superecommerce.data.remote.ApiInterface;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0011\u0010\u0005\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\b"}, d2 = {"Lcom/parvez/superecommerce/data/repository/test/remote/TestRemoteSourceImpl;", "Lcom/parvez/superecommerce/data/repository/test/remote/TestRemoteSource;", "apiInterface", "Lcom/parvez/superecommerce/data/remote/ApiInterface;", "(Lcom/parvez/superecommerce/data/remote/ApiInterface;)V", "test", "Lcom/parvez/superecommerce/data/model/statuscheck/StatusCheck;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class TestRemoteSourceImpl implements com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource {
    private final com.parvez.superecommerce.data.remote.ApiInterface apiInterface = null;
    
    @javax.inject.Inject
    public TestRemoteSourceImpl(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public java.lang.Object test(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.statuscheck.StatusCheck> continuation) {
        return null;
    }
}