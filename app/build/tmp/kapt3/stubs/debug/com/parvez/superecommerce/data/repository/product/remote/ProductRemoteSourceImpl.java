package com.parvez.superecommerce.data.repository.product.remote;

import com.parvez.superecommerce.data.model.product.Product;
import com.parvez.superecommerce.data.model.product.ProductListResponse;
import com.parvez.superecommerce.data.remote.ApiInterface;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0019\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\r"}, d2 = {"Lcom/parvez/superecommerce/data/repository/product/remote/ProductRemoteSourceImpl;", "Lcom/parvez/superecommerce/data/repository/product/remote/ProductRemoteSource;", "apiInterface", "Lcom/parvez/superecommerce/data/remote/ApiInterface;", "(Lcom/parvez/superecommerce/data/remote/ApiInterface;)V", "getProduct", "Lcom/parvez/superecommerce/data/model/product/Product;", "productId", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getProducts", "Lcom/parvez/superecommerce/data/model/product/ProductListResponse;", "category", "app_debug"})
public final class ProductRemoteSourceImpl implements com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource {
    private final com.parvez.superecommerce.data.remote.ApiInterface apiInterface = null;
    
    @javax.inject.Inject
    public ProductRemoteSourceImpl(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public java.lang.Object getProducts(@org.jetbrains.annotations.NotNull
    java.lang.String category, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.ProductListResponse> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public java.lang.Object getProduct(@org.jetbrains.annotations.NotNull
    java.lang.String productId, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.Product> continuation) {
        return null;
    }
}