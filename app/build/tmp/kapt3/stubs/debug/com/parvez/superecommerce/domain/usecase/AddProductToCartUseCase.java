package com.parvez.superecommerce.domain.usecase;

import com.parvez.superecommerce.data.model.product.Product;
import com.parvez.superecommerce.data.remote.NetworkResponse;
import com.parvez.superecommerce.data.repository.cart.CartRepository;
import kotlinx.coroutines.flow.Flow;
import retrofit2.HttpException;
import java.io.IOException;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00062\u0006\u0010\t\u001a\u00020\bH\u0086\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/parvez/superecommerce/domain/usecase/AddProductToCartUseCase;", "", "cartRepository", "Lcom/parvez/superecommerce/data/repository/cart/CartRepository;", "(Lcom/parvez/superecommerce/data/repository/cart/CartRepository;)V", "invoke", "Lkotlinx/coroutines/flow/Flow;", "Lcom/parvez/superecommerce/data/remote/NetworkResponse;", "Lcom/parvez/superecommerce/data/model/product/Product;", "product", "app_debug"})
public final class AddProductToCartUseCase {
    private final com.parvez.superecommerce.data.repository.cart.CartRepository cartRepository = null;
    
    @javax.inject.Inject
    public AddProductToCartUseCase(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.repository.cart.CartRepository cartRepository) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final kotlinx.coroutines.flow.Flow<com.parvez.superecommerce.data.remote.NetworkResponse<com.parvez.superecommerce.data.model.product.Product>> invoke(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.model.product.Product product) {
        return null;
    }
}