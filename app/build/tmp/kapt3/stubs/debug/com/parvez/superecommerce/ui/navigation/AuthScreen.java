package com.parvez.superecommerce.ui.navigation;

import java.lang.System;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b7\u0018\u00002\u00020\u0001:\u0005\u0007\b\t\n\u000bB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0005\f\r\u000e\u000f\u0010\u00a8\u0006\u0011"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AuthScreen;", "", "route", "", "(Ljava/lang/String;)V", "getRoute", "()Ljava/lang/String;", "ForgotPassword", "Login", "Onboarding", "OtpVerification", "Signup", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen$ForgotPassword;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen$Login;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen$Onboarding;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen$OtpVerification;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen$Signup;", "app_debug"})
public abstract class AuthScreen {
    @org.jetbrains.annotations.NotNull
    private final java.lang.String route = null;
    
    private AuthScreen(java.lang.String route) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getRoute() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AuthScreen$Onboarding;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen;", "()V", "app_debug"})
    public static final class Onboarding extends com.parvez.superecommerce.ui.navigation.AuthScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.AuthScreen.Onboarding INSTANCE = null;
        
        private Onboarding() {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AuthScreen$Login;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen;", "()V", "app_debug"})
    public static final class Login extends com.parvez.superecommerce.ui.navigation.AuthScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.AuthScreen.Login INSTANCE = null;
        
        private Login() {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AuthScreen$Signup;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen;", "()V", "app_debug"})
    public static final class Signup extends com.parvez.superecommerce.ui.navigation.AuthScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.AuthScreen.Signup INSTANCE = null;
        
        private Signup() {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AuthScreen$ForgotPassword;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen;", "()V", "app_debug"})
    public static final class ForgotPassword extends com.parvez.superecommerce.ui.navigation.AuthScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.AuthScreen.ForgotPassword INSTANCE = null;
        
        private ForgotPassword() {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AuthScreen$OtpVerification;", "Lcom/parvez/superecommerce/ui/navigation/AuthScreen;", "()V", "app_debug"})
    public static final class OtpVerification extends com.parvez.superecommerce.ui.navigation.AuthScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.AuthScreen.OtpVerification INSTANCE = null;
        
        private OtpVerification() {
            super(null);
        }
    }
}