package com.parvez.superecommerce.ui.navigation.bottomnavigation;

import androidx.compose.foundation.layout.RowScope;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Modifier;
import androidx.navigation.NavDestination;
import androidx.navigation.NavHostController;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u00012\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\u0010\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a&\u0010\u0005\u001a\u00020\u0001*\u00020\u00062\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0007\u00a8\u0006\u000b"}, d2 = {"MainScreen", "", "navController", "Landroidx/navigation/NavHostController;", "MyBottomBar", "AddItem", "Landroidx/compose/foundation/layout/RowScope;", "screen", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen;", "currentDestination", "Landroidx/navigation/NavDestination;", "app_debug"})
public final class MainScreenKt {
    
    @androidx.compose.runtime.Composable
    public static final void MainScreen(@org.jetbrains.annotations.NotNull
    androidx.navigation.NavHostController navController) {
    }
    
    @androidx.compose.runtime.Composable
    public static final void MyBottomBar(@org.jetbrains.annotations.NotNull
    androidx.navigation.NavHostController navController) {
    }
    
    @androidx.compose.runtime.Composable
    public static final void AddItem(@org.jetbrains.annotations.NotNull
    androidx.compose.foundation.layout.RowScope $this$AddItem, @org.jetbrains.annotations.NotNull
    androidx.navigation.NavHostController navController, @org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen screen, @org.jetbrains.annotations.Nullable
    androidx.navigation.NavDestination currentDestination) {
    }
}