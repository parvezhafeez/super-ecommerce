package com.parvez.superecommerce.ui.onboarding.composable;

import androidx.compose.foundation.layout.Arrangement;
import androidx.compose.foundation.layout.BoxScope;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\u001c\u0010\u0004\u001a\u00020\u0001*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0007H\u0007\u00a8\u0006\t"}, d2 = {"Indicator", "", "isSelected", "", "Indicators", "Landroidx/compose/foundation/layout/BoxScope;", "size", "", "index", "app_debug"})
public final class IndicatorsKt {
    
    @androidx.compose.runtime.Composable
    public static final void Indicators(@org.jetbrains.annotations.NotNull
    androidx.compose.foundation.layout.BoxScope $this$Indicators, int size, int index) {
    }
    
    @androidx.compose.runtime.Composable
    public static final void Indicator(boolean isSelected) {
    }
}