package com.parvez.superecommerce.ui.productlist;

import androidx.compose.runtime.State;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import com.parvez.superecommerce.data.model.product.ProductListResponse;
import com.parvez.superecommerce.data.remote.NetworkResponse;
import com.parvez.superecommerce.domain.usecase.GetProductListUseCase;
import com.parvez.superecommerce.ui.utils.Constants;
import dagger.hilt.android.lifecycle.HiltViewModel;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\n\u001a\u00020\u000bH\u0002R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\u000f8F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\u0014"}, d2 = {"Lcom/parvez/superecommerce/ui/productlist/ProductListViewModel;", "Landroidx/lifecycle/ViewModel;", "savedStateHandle", "Landroidx/lifecycle/SavedStateHandle;", "productListUseCase", "Lcom/parvez/superecommerce/domain/usecase/GetProductListUseCase;", "(Landroidx/lifecycle/SavedStateHandle;Lcom/parvez/superecommerce/domain/usecase/GetProductListUseCase;)V", "_state", "Landroidx/compose/runtime/MutableState;", "Lcom/parvez/superecommerce/ui/productlist/ProductListUiState;", "category", "", "getCategory", "()Ljava/lang/String;", "state", "Landroidx/compose/runtime/State;", "getState", "()Landroidx/compose/runtime/State;", "getProductList", "", "app_debug"})
public final class ProductListViewModel extends androidx.lifecycle.ViewModel {
    private final com.parvez.superecommerce.domain.usecase.GetProductListUseCase productListUseCase = null;
    @org.jetbrains.annotations.NotNull
    private final java.lang.String category = null;
    private final androidx.compose.runtime.MutableState<com.parvez.superecommerce.ui.productlist.ProductListUiState> _state = null;
    
    @javax.inject.Inject
    public ProductListViewModel(@org.jetbrains.annotations.NotNull
    androidx.lifecycle.SavedStateHandle savedStateHandle, @org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.domain.usecase.GetProductListUseCase productListUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getCategory() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.compose.runtime.State<com.parvez.superecommerce.ui.productlist.ProductListUiState> getState() {
        return null;
    }
    
    private final void getProductList(java.lang.String category) {
    }
}