package com.parvez.superecommerce.ui.login.composable;

import androidx.compose.runtime.Composable;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.graphics.Brush;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u00a8\u0006\u0004"}, d2 = {"OrLeftLine", "", "modifier", "Landroidx/compose/ui/Modifier;", "app_debug"})
public final class OrLeftLineKt {
    
    @androidx.compose.runtime.Composable
    public static final void OrLeftLine(@org.jetbrains.annotations.NotNull
    androidx.compose.ui.Modifier modifier) {
    }
}