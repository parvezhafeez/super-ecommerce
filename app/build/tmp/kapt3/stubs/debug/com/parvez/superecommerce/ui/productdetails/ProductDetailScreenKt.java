package com.parvez.superecommerce.ui.productdetails;

import android.widget.Toast;
import androidx.compose.material.icons.Icons;
import androidx.compose.material3.ExperimentalMaterial3Api;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.layout.ContentScale;
import androidx.compose.ui.tooling.preview.Preview;
import androidx.navigation.NavController;
import coil.request.ImageRequest;
import com.parvez.superecommerce.R;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u001a\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u0007\u001a\b\u0010\u0006\u001a\u00020\u0001H\u0007\u00a8\u0006\u0007"}, d2 = {"ProductDetails", "", "navController", "Landroidx/navigation/NavController;", "productDetailViewModel", "Lcom/parvez/superecommerce/ui/productdetails/ProductDetailViewModel;", "ProductDetailsPreview", "app_debug"})
public final class ProductDetailScreenKt {
    
    @androidx.compose.runtime.Composable
    @kotlin.OptIn(markerClass = {androidx.compose.material3.ExperimentalMaterial3Api.class})
    public static final void ProductDetails(@org.jetbrains.annotations.NotNull
    androidx.navigation.NavController navController, @org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.ui.productdetails.ProductDetailViewModel productDetailViewModel) {
    }
    
    @androidx.compose.runtime.Composable
    @androidx.compose.ui.tooling.preview.Preview(showBackground = true)
    public static final void ProductDetailsPreview() {
    }
}