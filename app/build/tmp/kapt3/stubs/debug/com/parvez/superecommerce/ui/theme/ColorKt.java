package com.parvez.superecommerce.ui.theme;

import java.lang.System;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\"\u0016\u0010\u0000\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0002\u0010\u0003\"\u0016\u0010\u0005\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0006\u0010\u0003\"\u0016\u0010\u0007\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\b\u0010\u0003\"\u0016\u0010\t\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\n\u0010\u0003\"\u0016\u0010\u000b\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\f\u0010\u0003\"\u0016\u0010\r\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u000e\u0010\u0003\"\u0016\u0010\u000f\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0010\u0010\u0003\"\u0016\u0010\u0011\u001a\u00020\u0001\u00f8\u0001\u0000\u00a2\u0006\n\n\u0002\u0010\u0004\u001a\u0004\b\u0012\u0010\u0003\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0013"}, d2 = {"Black", "Landroidx/compose/ui/graphics/Color;", "getBlack", "()J", "J", "BlueText", "getBlueText", "DarkGreen", "getDarkGreen", "GoldRating", "getGoldRating", "LightGreen", "getLightGreen", "TextGray", "getTextGray", "TextPlaceholderColor", "getTextPlaceholderColor", "White", "getWhite", "app_debug"})
public final class ColorKt {
    private static final long Black = 0L;
    private static final long White = 0L;
    private static final long TextGray = 0L;
    private static final long DarkGreen = 0L;
    private static final long LightGreen = 0L;
    private static final long BlueText = 0L;
    private static final long TextPlaceholderColor = 0L;
    private static final long GoldRating = 0L;
    
    public static final long getBlack() {
        return 0L;
    }
    
    public static final long getWhite() {
        return 0L;
    }
    
    public static final long getTextGray() {
        return 0L;
    }
    
    public static final long getDarkGreen() {
        return 0L;
    }
    
    public static final long getLightGreen() {
        return 0L;
    }
    
    public static final long getBlueText() {
        return 0L;
    }
    
    public static final long getTextPlaceholderColor() {
        return 0L;
    }
    
    public static final long getGoldRating() {
        return 0L;
    }
}