package com.parvez.superecommerce.ui.productdetails;

import android.util.Log;
import androidx.compose.runtime.State;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import com.parvez.superecommerce.data.model.product.Product;
import com.parvez.superecommerce.data.remote.NetworkResponse;
import com.parvez.superecommerce.domain.usecase.AddProductToCartUseCase;
import com.parvez.superecommerce.domain.usecase.GetProductDetailUseCase;
import com.parvez.superecommerce.ui.utils.Constants;
import dagger.hilt.android.lifecycle.HiltViewModel;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u000e\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018J\b\u0010\u0019\u001a\u00020\u0016H\u0002R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000e8F\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000e8F\u00a2\u0006\u0006\u001a\u0004\b\u0014\u0010\u0010\u00a8\u0006\u001a"}, d2 = {"Lcom/parvez/superecommerce/ui/productdetails/ProductDetailViewModel;", "Landroidx/lifecycle/ViewModel;", "savedStateHandle", "Landroidx/lifecycle/SavedStateHandle;", "addProductToCartUseCase", "Lcom/parvez/superecommerce/domain/usecase/AddProductToCartUseCase;", "productDetailUseCase", "Lcom/parvez/superecommerce/domain/usecase/GetProductDetailUseCase;", "(Landroidx/lifecycle/SavedStateHandle;Lcom/parvez/superecommerce/domain/usecase/AddProductToCartUseCase;Lcom/parvez/superecommerce/domain/usecase/GetProductDetailUseCase;)V", "_addToCartState", "Landroidx/compose/runtime/MutableState;", "Lcom/parvez/superecommerce/ui/productdetails/ProductDetailUiState;", "_state", "addToCartState", "Landroidx/compose/runtime/State;", "getAddToCartState", "()Landroidx/compose/runtime/State;", "productId", "", "state", "getState", "addProductToCart", "", "product", "Lcom/parvez/superecommerce/data/model/product/Product;", "getProductDetail", "app_debug"})
public final class ProductDetailViewModel extends androidx.lifecycle.ViewModel {
    private final com.parvez.superecommerce.domain.usecase.AddProductToCartUseCase addProductToCartUseCase = null;
    private final com.parvez.superecommerce.domain.usecase.GetProductDetailUseCase productDetailUseCase = null;
    private final java.lang.String productId = null;
    private final androidx.compose.runtime.MutableState<com.parvez.superecommerce.ui.productdetails.ProductDetailUiState> _state = null;
    private final androidx.compose.runtime.MutableState<com.parvez.superecommerce.ui.productdetails.ProductDetailUiState> _addToCartState = null;
    
    @javax.inject.Inject
    public ProductDetailViewModel(@org.jetbrains.annotations.NotNull
    androidx.lifecycle.SavedStateHandle savedStateHandle, @org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.domain.usecase.AddProductToCartUseCase addProductToCartUseCase, @org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.domain.usecase.GetProductDetailUseCase productDetailUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.compose.runtime.State<com.parvez.superecommerce.ui.productdetails.ProductDetailUiState> getState() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.compose.runtime.State<com.parvez.superecommerce.ui.productdetails.ProductDetailUiState> getAddToCartState() {
        return null;
    }
    
    private final void getProductDetail() {
    }
    
    public final void addProductToCart(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.model.product.Product product) {
    }
}