package com.parvez.superecommerce.data.repository.cart.remote;

import com.parvez.superecommerce.data.model.cart.CartResponse;
import com.parvez.superecommerce.data.model.product.Product;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0019\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J\u0011\u0010\u0006\u001a\u00020\u0007H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\b\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\t"}, d2 = {"Lcom/parvez/superecommerce/data/repository/cart/remote/CartRemoteSource;", "", "addProductToCart", "Lcom/parvez/superecommerce/data/model/product/Product;", "product", "(Lcom/parvez/superecommerce/data/model/product/Product;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getCart", "Lcom/parvez/superecommerce/data/model/cart/CartResponse;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface CartRemoteSource {
    
    @org.jetbrains.annotations.Nullable
    public abstract java.lang.Object addProductToCart(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.model.product.Product product, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.Product> continuation);
    
    @org.jetbrains.annotations.Nullable
    public abstract java.lang.Object getCart(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.cart.CartResponse> continuation);
}