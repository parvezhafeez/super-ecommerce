package com.parvez.superecommerce.ui.settings;

import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.tooling.preview.Preview;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u001a\b\u0010\u0000\u001a\u00020\u0001H\u0007\u001a\b\u0010\u0002\u001a\u00020\u0001H\u0007\u00a8\u0006\u0003"}, d2 = {"SettingScreen", "", "SettingScreenPreview", "app_debug"})
public final class SettingScreenKt {
    
    @androidx.compose.runtime.Composable
    public static final void SettingScreen() {
    }
    
    @androidx.compose.runtime.Composable
    @androidx.compose.ui.tooling.preview.Preview
    public static final void SettingScreenPreview() {
    }
}