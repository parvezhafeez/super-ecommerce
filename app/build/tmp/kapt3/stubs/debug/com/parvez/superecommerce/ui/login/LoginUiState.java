package com.parvez.superecommerce.ui.login;

import androidx.compose.runtime.State;
import androidx.lifecycle.ViewModel;
import com.parvez.superecommerce.data.model.login.LoginResponse;
import com.parvez.superecommerce.data.prefdatastore.DataStoreManager;
import com.parvez.superecommerce.data.remote.NetworkResponse;
import com.parvez.superecommerce.domain.usecase.LoginUseCase;
import dagger.hilt.android.lifecycle.HiltViewModel;
import kotlinx.coroutines.Deferred;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\r\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\'\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J+\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00032\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0007H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\r\u00a8\u0006\u0017"}, d2 = {"Lcom/parvez/superecommerce/ui/login/LoginUiState;", "", "isLoading", "", "data", "Lcom/parvez/superecommerce/data/model/login/LoginResponse;", "error", "", "(ZLcom/parvez/superecommerce/data/model/login/LoginResponse;Ljava/lang/String;)V", "getData", "()Lcom/parvez/superecommerce/data/model/login/LoginResponse;", "getError", "()Ljava/lang/String;", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "app_debug"})
public final class LoginUiState {
    private final boolean isLoading = false;
    @org.jetbrains.annotations.Nullable
    private final com.parvez.superecommerce.data.model.login.LoginResponse data = null;
    @org.jetbrains.annotations.Nullable
    private final java.lang.String error = null;
    
    @org.jetbrains.annotations.NotNull
    public final com.parvez.superecommerce.ui.login.LoginUiState copy(boolean isLoading, @org.jetbrains.annotations.Nullable
    com.parvez.superecommerce.data.model.login.LoginResponse data, @org.jetbrains.annotations.Nullable
    java.lang.String error) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public LoginUiState() {
        super();
    }
    
    public LoginUiState(boolean isLoading, @org.jetbrains.annotations.Nullable
    com.parvez.superecommerce.data.model.login.LoginResponse data, @org.jetbrains.annotations.Nullable
    java.lang.String error) {
        super();
    }
    
    public final boolean component1() {
        return false;
    }
    
    public final boolean isLoading() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable
    public final com.parvez.superecommerce.data.model.login.LoginResponse component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final com.parvez.superecommerce.data.model.login.LoginResponse getData() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.String getError() {
        return null;
    }
}