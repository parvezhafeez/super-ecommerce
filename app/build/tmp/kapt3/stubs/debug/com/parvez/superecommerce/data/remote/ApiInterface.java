package com.parvez.superecommerce.data.remote;

import com.parvez.superecommerce.data.model.cart.CartResponse;
import com.parvez.superecommerce.data.model.category.CategoryResponse;
import com.parvez.superecommerce.data.model.login.LoginResponse;
import com.parvez.superecommerce.data.model.post.PostResponse;
import com.parvez.superecommerce.data.model.product.Product;
import com.parvez.superecommerce.data.model.product.ProductListResponse;
import com.parvez.superecommerce.data.model.statuscheck.StatusCheck;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u001b\u0010\u0002\u001a\u00020\u00032\b\b\u0001\u0010\u0004\u001a\u00020\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0005J\u0011\u0010\u0006\u001a\u00020\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ\u0011\u0010\t\u001a\u00020\nH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ\u0011\u0010\u000b\u001a\u00020\fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ\u001b\u0010\r\u001a\u00020\u00032\b\b\u0001\u0010\u000e\u001a\u00020\u000fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J\u001b\u0010\u0011\u001a\u00020\u00122\b\b\u0001\u0010\u0013\u001a\u00020\u000fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J%\u0010\u0014\u001a\u00020\u00152\b\b\u0001\u0010\u0016\u001a\u00020\u000f2\b\b\u0001\u0010\u0017\u001a\u00020\u000fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0018J\u0011\u0010\u0019\u001a\u00020\u001aH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\b\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u001b"}, d2 = {"Lcom/parvez/superecommerce/data/remote/ApiInterface;", "", "addProductToCart", "Lcom/parvez/superecommerce/data/model/product/Product;", "product", "(Lcom/parvez/superecommerce/data/model/product/Product;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getCart", "Lcom/parvez/superecommerce/data/model/cart/CartResponse;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getCategories", "Lcom/parvez/superecommerce/data/model/category/CategoryResponse;", "getPosts", "Lcom/parvez/superecommerce/data/model/post/PostResponse;", "getProduct", "productId", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getProducts", "Lcom/parvez/superecommerce/data/model/product/ProductListResponse;", "category", "login", "Lcom/parvez/superecommerce/data/model/login/LoginResponse;", "username", "password", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "test", "Lcom/parvez/superecommerce/data/model/statuscheck/StatusCheck;", "app_debug"})
public abstract interface ApiInterface {
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.GET(value = "/test")
    public abstract java.lang.Object test(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.statuscheck.StatusCheck> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "/auth/login")
    @retrofit2.http.FormUrlEncoded
    public abstract java.lang.Object login(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Field(value = "username")
    java.lang.String username, @org.jetbrains.annotations.NotNull
    @retrofit2.http.Field(value = "password")
    java.lang.String password, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.login.LoginResponse> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.GET(value = "/products/categories")
    public abstract java.lang.Object getCategories(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.category.CategoryResponse> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.GET(value = "/products/category/{category}")
    public abstract java.lang.Object getProducts(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Path(value = "category")
    java.lang.String category, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.ProductListResponse> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.GET(value = "/products/{productId}")
    public abstract java.lang.Object getProduct(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Path(value = "productId")
    java.lang.String productId, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.Product> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.POST(value = "/products/add")
    public abstract java.lang.Object addProductToCart(@org.jetbrains.annotations.NotNull
    @retrofit2.http.Body
    com.parvez.superecommerce.data.model.product.Product product, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.Product> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.GET(value = "/carts/1")
    public abstract java.lang.Object getCart(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.cart.CartResponse> continuation);
    
    @org.jetbrains.annotations.Nullable
    @retrofit2.http.GET(value = "/posts")
    public abstract java.lang.Object getPosts(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.post.PostResponse> continuation);
}