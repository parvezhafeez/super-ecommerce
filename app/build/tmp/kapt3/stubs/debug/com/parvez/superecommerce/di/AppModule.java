package com.parvez.superecommerce.di;

import com.parvez.superecommerce.data.remote.ApiInterface;
import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource;
import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSourceImpl;
import com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSource;
import com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSourceImpl;
import com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSource;
import com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSourceImpl;
import com.parvez.superecommerce.data.repository.post.remote.PostRemoteSource;
import com.parvez.superecommerce.data.repository.post.remote.PostRemoteSourceImpl;
import com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource;
import com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSourceImpl;
import com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource;
import com.parvez.superecommerce.data.repository.test.remote.TestRemoteSourceImpl;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import javax.inject.Singleton;

@dagger.hilt.InstallIn(value = {dagger.hilt.components.SingletonComponent.class})
@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/parvez/superecommerce/di/AppModule;", "", "()V", "provideCartRemoteSource", "Lcom/parvez/superecommerce/data/repository/cart/remote/CartRemoteSource;", "apiInterface", "Lcom/parvez/superecommerce/data/remote/ApiInterface;", "provideCategoryRemoteSource", "Lcom/parvez/superecommerce/data/repository/category/remote/CategoryRemoteSource;", "provideLoginRemoteSource", "Lcom/parvez/superecommerce/data/repository/login/remote/LoginRemoteSource;", "providePostRemoteSource", "Lcom/parvez/superecommerce/data/repository/post/remote/PostRemoteSource;", "provideProductRemoteSource", "Lcom/parvez/superecommerce/data/repository/product/remote/ProductRemoteSource;", "provideTestRemoteSource", "Lcom/parvez/superecommerce/data/repository/test/remote/TestRemoteSource;", "app_debug"})
@dagger.Module
public final class AppModule {
    @org.jetbrains.annotations.NotNull
    public static final com.parvez.superecommerce.di.AppModule INSTANCE = null;
    
    private AppModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource provideTestRemoteSource(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSource provideLoginRemoteSource(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSource provideCategoryRemoteSource(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource provideProductRemoteSource(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @dagger.Provides
    @javax.inject.Singleton
    public final com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource provideCartRemoteSource(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    @javax.inject.Singleton
    @dagger.Provides
    public final com.parvez.superecommerce.data.repository.post.remote.PostRemoteSource providePostRemoteSource(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        return null;
    }
}