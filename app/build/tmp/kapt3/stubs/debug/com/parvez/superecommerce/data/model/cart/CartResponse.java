package com.parvez.superecommerce.data.model.cart;

import java.lang.System;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0087\b\u0018\u00002\u00020\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\fJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0003H\u00c6\u0003JU\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010!\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\"\u001a\u00020#H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000eR\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000eR\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000e\u00a8\u0006$"}, d2 = {"Lcom/parvez/superecommerce/data/model/cart/CartResponse;", "", "discountedTotal", "", "id", "products", "", "Lcom/parvez/superecommerce/data/model/cart/CartProduct;", "total", "totalProducts", "totalQuantity", "userId", "(IILjava/util/List;IIII)V", "getDiscountedTotal", "()I", "getId", "getProducts", "()Ljava/util/List;", "getTotal", "getTotalProducts", "getTotalQuantity", "getUserId", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "", "other", "hashCode", "toString", "", "app_debug"})
public final class CartResponse {
    private final int discountedTotal = 0;
    private final int id = 0;
    @org.jetbrains.annotations.NotNull
    private final java.util.List<com.parvez.superecommerce.data.model.cart.CartProduct> products = null;
    private final int total = 0;
    private final int totalProducts = 0;
    private final int totalQuantity = 0;
    private final int userId = 0;
    
    @org.jetbrains.annotations.NotNull
    public final com.parvez.superecommerce.data.model.cart.CartResponse copy(int discountedTotal, int id, @org.jetbrains.annotations.NotNull
    java.util.List<com.parvez.superecommerce.data.model.cart.CartProduct> products, int total, int totalProducts, int totalQuantity, int userId) {
        return null;
    }
    
    @java.lang.Override
    public boolean equals(@org.jetbrains.annotations.Nullable
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    @java.lang.Override
    public java.lang.String toString() {
        return null;
    }
    
    public CartResponse(int discountedTotal, int id, @org.jetbrains.annotations.NotNull
    java.util.List<com.parvez.superecommerce.data.model.cart.CartProduct> products, int total, int totalProducts, int totalQuantity, int userId) {
        super();
    }
    
    public final int component1() {
        return 0;
    }
    
    public final int getDiscountedTotal() {
        return 0;
    }
    
    public final int component2() {
        return 0;
    }
    
    public final int getId() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<com.parvez.superecommerce.data.model.cart.CartProduct> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.util.List<com.parvez.superecommerce.data.model.cart.CartProduct> getProducts() {
        return null;
    }
    
    public final int component4() {
        return 0;
    }
    
    public final int getTotal() {
        return 0;
    }
    
    public final int component5() {
        return 0;
    }
    
    public final int getTotalProducts() {
        return 0;
    }
    
    public final int component6() {
        return 0;
    }
    
    public final int getTotalQuantity() {
        return 0;
    }
    
    public final int component7() {
        return 0;
    }
    
    public final int getUserId() {
        return 0;
    }
}