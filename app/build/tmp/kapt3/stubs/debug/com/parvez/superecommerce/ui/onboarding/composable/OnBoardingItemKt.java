package com.parvez.superecommerce.ui.onboarding.composable;

import androidx.compose.foundation.layout.Arrangement;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Alignment;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.font.FontWeight;
import androidx.compose.ui.text.style.TextAlign;
import androidx.compose.ui.tooling.preview.Preview;
import com.parvez.superecommerce.R;
import com.parvez.superecommerce.ui.onboarding.model.OnBoardingItems;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\b\u0010\u0004\u001a\u00020\u0001H\u0007\u00a8\u0006\u0005"}, d2 = {"OnBoardingItem", "", "item", "Lcom/parvez/superecommerce/ui/onboarding/model/OnBoardingItems;", "OnBoardingItemPreview", "app_debug"})
public final class OnBoardingItemKt {
    
    @androidx.compose.runtime.Composable
    public static final void OnBoardingItem(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.ui.onboarding.model.OnBoardingItems item) {
    }
    
    @androidx.compose.runtime.Composable
    @androidx.compose.ui.tooling.preview.Preview(showSystemUi = true)
    public static final void OnBoardingItemPreview() {
    }
}