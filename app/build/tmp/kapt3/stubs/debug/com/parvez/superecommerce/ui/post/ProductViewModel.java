package com.parvez.superecommerce.ui.post;

import androidx.compose.runtime.State;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import com.parvez.superecommerce.data.model.post.PostResponse;
import com.parvez.superecommerce.data.remote.NetworkResponse;
import com.parvez.superecommerce.domain.usecase.GetPostUseCase;
import dagger.hilt.android.lifecycle.HiltViewModel;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel
@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u0010\u000e\u001a\u00020\u000fH\u0002R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u000b8F\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\r\u00a8\u0006\u0010"}, d2 = {"Lcom/parvez/superecommerce/ui/post/ProductViewModel;", "Landroidx/lifecycle/ViewModel;", "savedStateHandle", "Landroidx/lifecycle/SavedStateHandle;", "getPostUseCase", "Lcom/parvez/superecommerce/domain/usecase/GetPostUseCase;", "(Landroidx/lifecycle/SavedStateHandle;Lcom/parvez/superecommerce/domain/usecase/GetPostUseCase;)V", "_state", "Landroidx/compose/runtime/MutableState;", "Lcom/parvez/superecommerce/ui/post/PostUiState;", "state", "Landroidx/compose/runtime/State;", "getState", "()Landroidx/compose/runtime/State;", "getPosts", "", "app_debug"})
public final class ProductViewModel extends androidx.lifecycle.ViewModel {
    private final com.parvez.superecommerce.domain.usecase.GetPostUseCase getPostUseCase = null;
    private final androidx.compose.runtime.MutableState<com.parvez.superecommerce.ui.post.PostUiState> _state = null;
    
    @javax.inject.Inject
    public ProductViewModel(@org.jetbrains.annotations.NotNull
    androidx.lifecycle.SavedStateHandle savedStateHandle, @org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.domain.usecase.GetPostUseCase getPostUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.compose.runtime.State<com.parvez.superecommerce.ui.post.PostUiState> getState() {
        return null;
    }
    
    private final void getPosts() {
    }
}