package com.parvez.superecommerce.ui.signup;

import androidx.compose.material.icons.Icons;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.text.input.KeyboardType;
import androidx.compose.ui.tooling.preview.Preview;
import androidx.navigation.NavController;
import com.parvez.superecommerce.R;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\b\u0010\u0004\u001a\u00020\u0001H\u0007\u00a8\u0006\u0005"}, d2 = {"SignupScreen", "", "navController", "Landroidx/navigation/NavController;", "SignupScreenPreview", "app_debug"})
public final class SignupScreenKt {
    
    @androidx.compose.runtime.Composable
    public static final void SignupScreen(@org.jetbrains.annotations.NotNull
    androidx.navigation.NavController navController) {
    }
    
    @androidx.compose.runtime.Composable
    @androidx.compose.ui.tooling.preview.Preview(showBackground = true)
    public static final void SignupScreenPreview() {
    }
}