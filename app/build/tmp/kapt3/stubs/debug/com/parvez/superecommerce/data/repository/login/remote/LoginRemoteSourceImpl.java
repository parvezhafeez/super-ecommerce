package com.parvez.superecommerce.data.repository.login.remote;

import com.parvez.superecommerce.data.model.login.LoginResponse;
import com.parvez.superecommerce.data.remote.ApiInterface;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J!\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000b"}, d2 = {"Lcom/parvez/superecommerce/data/repository/login/remote/LoginRemoteSourceImpl;", "Lcom/parvez/superecommerce/data/repository/login/remote/LoginRemoteSource;", "apiInterface", "Lcom/parvez/superecommerce/data/remote/ApiInterface;", "(Lcom/parvez/superecommerce/data/remote/ApiInterface;)V", "login", "Lcom/parvez/superecommerce/data/model/login/LoginResponse;", "username", "", "password", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class LoginRemoteSourceImpl implements com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSource {
    private final com.parvez.superecommerce.data.remote.ApiInterface apiInterface = null;
    
    @javax.inject.Inject
    public LoginRemoteSourceImpl(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.remote.ApiInterface apiInterface) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    @java.lang.Override
    public java.lang.Object login(@org.jetbrains.annotations.NotNull
    java.lang.String username, @org.jetbrains.annotations.NotNull
    java.lang.String password, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.login.LoginResponse> continuation) {
        return null;
    }
}