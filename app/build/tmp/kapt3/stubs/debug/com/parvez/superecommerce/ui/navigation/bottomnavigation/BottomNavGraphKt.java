package com.parvez.superecommerce.ui.navigation.bottomnavigation;

import androidx.compose.runtime.Composable;
import androidx.navigation.NavHostController;
import com.parvez.superecommerce.ui.navigation.navgraph.Graph;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u00a8\u0006\u0004"}, d2 = {"BottomNavGraph", "", "navController", "Landroidx/navigation/NavHostController;", "app_debug"})
public final class BottomNavGraphKt {
    
    @androidx.compose.runtime.Composable
    public static final void BottomNavGraph(@org.jetbrains.annotations.NotNull
    androidx.navigation.NavHostController navController) {
    }
}