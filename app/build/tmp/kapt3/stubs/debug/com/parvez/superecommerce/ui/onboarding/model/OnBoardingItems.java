package com.parvez.superecommerce.ui.onboarding.model;

import com.parvez.superecommerce.R;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\t\b\u0007\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b\u00a8\u0006\f"}, d2 = {"Lcom/parvez/superecommerce/ui/onboarding/model/OnBoardingItems;", "", "image", "", "title", "desc", "(III)V", "getDesc", "()I", "getImage", "getTitle", "Companion", "app_debug"})
public final class OnBoardingItems {
    private final int image = 0;
    private final int title = 0;
    private final int desc = 0;
    @org.jetbrains.annotations.NotNull
    public static final com.parvez.superecommerce.ui.onboarding.model.OnBoardingItems.Companion Companion = null;
    
    public OnBoardingItems(int image, int title, int desc) {
        super();
    }
    
    public final int getImage() {
        return 0;
    }
    
    public final int getTitle() {
        return 0;
    }
    
    public final int getDesc() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a8\u0006\u0006"}, d2 = {"Lcom/parvez/superecommerce/ui/onboarding/model/OnBoardingItems$Companion;", "", "()V", "getData", "", "Lcom/parvez/superecommerce/ui/onboarding/model/OnBoardingItems;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull
        public final java.util.List<com.parvez.superecommerce.ui.onboarding.model.OnBoardingItems> getData() {
            return null;
        }
    }
}