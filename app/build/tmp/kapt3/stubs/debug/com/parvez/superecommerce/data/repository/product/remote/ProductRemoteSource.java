package com.parvez.superecommerce.data.repository.product.remote;

import com.parvez.superecommerce.data.model.product.Product;
import com.parvez.superecommerce.data.model.product.ProductListResponse;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0019\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006J\u0019\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0005H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\n"}, d2 = {"Lcom/parvez/superecommerce/data/repository/product/remote/ProductRemoteSource;", "", "getProduct", "Lcom/parvez/superecommerce/data/model/product/Product;", "productId", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getProducts", "Lcom/parvez/superecommerce/data/model/product/ProductListResponse;", "category", "app_debug"})
public abstract interface ProductRemoteSource {
    
    @org.jetbrains.annotations.Nullable
    public abstract java.lang.Object getProducts(@org.jetbrains.annotations.NotNull
    java.lang.String category, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.ProductListResponse> continuation);
    
    @org.jetbrains.annotations.Nullable
    public abstract java.lang.Object getProduct(@org.jetbrains.annotations.NotNull
    java.lang.String productId, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.Product> continuation);
}