package com.parvez.superecommerce.ui.onboarding;

import androidx.compose.runtime.Composable;
import androidx.compose.ui.Modifier;
import androidx.navigation.NavController;
import com.google.accompanist.pager.ExperimentalPagerApi;
import com.google.accompanist.pager.PagerState;
import com.parvez.superecommerce.ui.navigation.AuthScreen;
import com.parvez.superecommerce.ui.onboarding.model.OnBoardingItems;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u00a8\u0006\u0004"}, d2 = {"OnBoarding", "", "navController", "Landroidx/navigation/NavController;", "app_debug"})
public final class OnBoardingKt {
    
    @androidx.compose.runtime.Composable
    @kotlin.OptIn(markerClass = {com.google.accompanist.pager.ExperimentalPagerApi.class})
    public static final void OnBoarding(@org.jetbrains.annotations.NotNull
    androidx.navigation.NavController navController) {
    }
}