package com.parvez.superecommerce.data.repository.cart;

import com.parvez.superecommerce.data.model.cart.CartResponse;
import com.parvez.superecommerce.data.model.product.Product;
import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\bJ\u0011\u0010\t\u001a\u00020\nH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\f"}, d2 = {"Lcom/parvez/superecommerce/data/repository/cart/CartRepository;", "", "cartRemoteSource", "Lcom/parvez/superecommerce/data/repository/cart/remote/CartRemoteSource;", "(Lcom/parvez/superecommerce/data/repository/cart/remote/CartRemoteSource;)V", "addProductToCart", "Lcom/parvez/superecommerce/data/model/product/Product;", "product", "(Lcom/parvez/superecommerce/data/model/product/Product;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getCart", "Lcom/parvez/superecommerce/data/model/cart/CartResponse;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class CartRepository {
    private final com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource cartRemoteSource = null;
    
    @javax.inject.Inject
    public CartRepository(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource cartRemoteSource) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Object addProductToCart(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.model.product.Product product, @org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.product.Product> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable
    public final java.lang.Object getCart(@org.jetbrains.annotations.NotNull
    kotlin.coroutines.Continuation<? super com.parvez.superecommerce.data.model.cart.CartResponse> continuation) {
        return null;
    }
}