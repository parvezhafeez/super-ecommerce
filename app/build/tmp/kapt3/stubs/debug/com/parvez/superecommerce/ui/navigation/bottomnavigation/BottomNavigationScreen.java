package com.parvez.superecommerce.ui.navigation.bottomnavigation;

import androidx.annotation.StringRes;
import androidx.compose.material.icons.Icons;
import androidx.compose.ui.graphics.vector.ImageVector;
import com.parvez.superecommerce.R;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b7\u0018\u00002\u00020\u0001:\u0004\u0011\u0012\u0013\u0014B)\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\b\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\r\u0082\u0001\u0004\u0015\u0016\u0017\u0018\u00a8\u0006\u0019"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen;", "", "route", "", "title", "", "selectedIcon", "Landroidx/compose/ui/graphics/vector/ImageVector;", "unselectedIcon", "(Ljava/lang/String;ILandroidx/compose/ui/graphics/vector/ImageVector;Landroidx/compose/ui/graphics/vector/ImageVector;)V", "getRoute", "()Ljava/lang/String;", "getSelectedIcon", "()Landroidx/compose/ui/graphics/vector/ImageVector;", "getTitle", "()I", "getUnselectedIcon", "Cart", "Home", "Post", "Setting", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen$Cart;", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen$Home;", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen$Post;", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen$Setting;", "app_debug"})
public abstract class BottomNavigationScreen {
    @org.jetbrains.annotations.NotNull
    private final java.lang.String route = null;
    private final int title = 0;
    @org.jetbrains.annotations.NotNull
    private final androidx.compose.ui.graphics.vector.ImageVector selectedIcon = null;
    @org.jetbrains.annotations.NotNull
    private final androidx.compose.ui.graphics.vector.ImageVector unselectedIcon = null;
    
    private BottomNavigationScreen(java.lang.String route, @androidx.annotation.StringRes
    int title, androidx.compose.ui.graphics.vector.ImageVector selectedIcon, androidx.compose.ui.graphics.vector.ImageVector unselectedIcon) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getRoute() {
        return null;
    }
    
    public final int getTitle() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.compose.ui.graphics.vector.ImageVector getSelectedIcon() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public final androidx.compose.ui.graphics.vector.ImageVector getUnselectedIcon() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen$Home;", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen;", "()V", "app_debug"})
    public static final class Home extends com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen.Home INSTANCE = null;
        
        private Home() {
            super(null, 0, null, null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen$Post;", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen;", "()V", "app_debug"})
    public static final class Post extends com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen.Post INSTANCE = null;
        
        private Post() {
            super(null, 0, null, null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen$Cart;", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen;", "()V", "app_debug"})
    public static final class Cart extends com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen.Cart INSTANCE = null;
        
        private Cart() {
            super(null, 0, null, null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen$Setting;", "Lcom/parvez/superecommerce/ui/navigation/bottomnavigation/BottomNavigationScreen;", "()V", "app_debug"})
    public static final class Setting extends com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.bottomnavigation.BottomNavigationScreen.Setting INSTANCE = null;
        
        private Setting() {
            super(null, 0, null, null);
        }
    }
}