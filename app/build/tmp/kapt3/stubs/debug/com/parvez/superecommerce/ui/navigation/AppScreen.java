package com.parvez.superecommerce.ui.navigation;

import com.parvez.superecommerce.ui.utils.Constants;

@kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b7\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AppScreen;", "", "route", "", "(Ljava/lang/String;)V", "getRoute", "()Ljava/lang/String;", "ProductDetail", "ProductList", "Lcom/parvez/superecommerce/ui/navigation/AppScreen$ProductDetail;", "Lcom/parvez/superecommerce/ui/navigation/AppScreen$ProductList;", "app_debug"})
public abstract class AppScreen {
    @org.jetbrains.annotations.NotNull
    private final java.lang.String route = null;
    
    private AppScreen(java.lang.String route) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull
    public final java.lang.String getRoute() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004\u00a8\u0006\u0006"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AppScreen$ProductList;", "Lcom/parvez/superecommerce/ui/navigation/AppScreen;", "()V", "withCategoryName", "", "category", "app_debug"})
    public static final class ProductList extends com.parvez.superecommerce.ui.navigation.AppScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.AppScreen.ProductList INSTANCE = null;
        
        private ProductList() {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull
        public final java.lang.String withCategoryName(@org.jetbrains.annotations.NotNull
        java.lang.String category) {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 8, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004\u00a8\u0006\u0006"}, d2 = {"Lcom/parvez/superecommerce/ui/navigation/AppScreen$ProductDetail;", "Lcom/parvez/superecommerce/ui/navigation/AppScreen;", "()V", "withProductId", "", "productId", "app_debug"})
    public static final class ProductDetail extends com.parvez.superecommerce.ui.navigation.AppScreen {
        @org.jetbrains.annotations.NotNull
        public static final com.parvez.superecommerce.ui.navigation.AppScreen.ProductDetail INSTANCE = null;
        
        private ProductDetail() {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull
        public final java.lang.String withProductId(@org.jetbrains.annotations.NotNull
        java.lang.String productId) {
            return null;
        }
    }
}