package com.parvez.superecommerce.ui.theme;

import androidx.compose.material3.Typography;
import androidx.compose.ui.text.TextStyle;
import androidx.compose.ui.text.font.FontWeight;
import com.parvez.superecommerce.R;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0003\"\u0011\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0003\u00a8\u0006\u0006"}, d2 = {"SuperTypography", "Landroidx/compose/material3/Typography;", "getSuperTypography", "()Landroidx/compose/material3/Typography;", "Typography", "getTypography", "app_debug"})
public final class TypeKt {
    @org.jetbrains.annotations.NotNull
    private static final androidx.compose.material3.Typography Typography = null;
    @org.jetbrains.annotations.NotNull
    private static final androidx.compose.material3.Typography SuperTypography = null;
    
    @org.jetbrains.annotations.NotNull
    public static final androidx.compose.material3.Typography getTypography() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull
    public static final androidx.compose.material3.Typography getSuperTypography() {
        return null;
    }
}