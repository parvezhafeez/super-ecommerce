package com.parvez.superecommerce.ui.post.composable;

import androidx.compose.material.icons.Icons;
import androidx.compose.runtime.Composable;
import androidx.compose.ui.Modifier;
import androidx.compose.ui.graphics.ColorFilter;
import androidx.compose.ui.layout.ContentScale;
import androidx.compose.ui.text.style.TextOverflow;
import androidx.compose.ui.tooling.preview.Preview;
import com.parvez.superecommerce.R;
import com.parvez.superecommerce.data.model.post.Post;

@kotlin.Metadata(mv = {1, 8, 0}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\b\u0010\u0004\u001a\u00020\u0001H\u0007\u00a8\u0006\u0005"}, d2 = {"PostItem", "", "post", "Lcom/parvez/superecommerce/data/model/post/Post;", "PostItemPreview", "app_debug"})
public final class PostItemKt {
    
    @androidx.compose.runtime.Composable
    public static final void PostItem(@org.jetbrains.annotations.NotNull
    com.parvez.superecommerce.data.model.post.Post post) {
    }
    
    @androidx.compose.runtime.Composable
    @androidx.compose.ui.tooling.preview.Preview
    public static final void PostItemPreview() {
    }
}