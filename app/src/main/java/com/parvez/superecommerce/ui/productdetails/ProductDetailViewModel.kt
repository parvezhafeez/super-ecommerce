package com.parvez.superecommerce.ui.productdetails

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.domain.usecase.AddProductToCartUseCase
import com.parvez.superecommerce.domain.usecase.GetProductDetailUseCase
import com.parvez.superecommerce.ui.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ProductDetailViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val addProductToCartUseCase: AddProductToCartUseCase,
    private val productDetailUseCase: GetProductDetailUseCase
) : ViewModel() {

    //    private val productId =  "1"
    private val productId = savedStateHandle.get<String>(Constants.NAV_ARG_PRODUCT_ID) ?: "1"

    private val _state = mutableStateOf<ProductDetailUiState>(ProductDetailUiState())
    val state: State<ProductDetailUiState>
        get() = _state

    private val _addToCartState = mutableStateOf<ProductDetailUiState>(ProductDetailUiState())
    val addToCartState: State<ProductDetailUiState>
        get() = _addToCartState

    init {
        getProductDetail()
    }

    private fun getProductDetail() {
        productDetailUseCase.invoke(productId).onEach { result ->
            when (result) {
                is NetworkResponse.Error -> {
                    _state.value = ProductDetailUiState(error = result.errorMessage)
                }

                is NetworkResponse.Loading -> {
                    _state.value = ProductDetailUiState(isLoading = true)
                }

                is NetworkResponse.Success -> {
                    _state.value = ProductDetailUiState(data = result.data)
                }
            }
        }.launchIn(viewModelScope)
    }

    fun addProductToCart(product: Product) {
        Log.d("TAG", "Log: 1 - add product to cart function")
        addProductToCartUseCase.invoke(product).onEach { result ->
            when (result) {
                is NetworkResponse.Error -> {
                    _addToCartState.value = ProductDetailUiState(error = result.errorMessage)
                }

                is NetworkResponse.Loading -> {
                    _addToCartState.value = ProductDetailUiState(isLoading = true)
                }

                is NetworkResponse.Success -> {
                    _addToCartState.value = ProductDetailUiState(data = result.data)
                }
            }
        }.launchIn(viewModelScope)
    }

}

data class ProductDetailUiState(
    val isLoading: Boolean = false,
    val data: Product? = null,
    val error: String? = ""
)


