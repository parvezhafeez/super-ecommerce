package com.parvez.superecommerce.ui.navigation.bottomnavigation

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.parvez.superecommerce.ui.theme.Black
import com.parvez.superecommerce.ui.theme.TextPlaceholderColor

@Composable
fun MainScreen(
    navController: NavHostController = rememberNavController()
) {

    Scaffold(
        bottomBar = {
            MyBottomBar(navController = navController)
        },
    ) { innerPadding ->
        Box(
            modifier = Modifier
                .padding(innerPadding)
        ) {
            BottomNavGraph(
                navController = navController
            )
        }
    }

}


@Composable
fun MyBottomBar(
    navController: NavHostController
) {

    val screenList = listOf(
        BottomNavigationScreen.Home,
        BottomNavigationScreen.Post,
        BottomNavigationScreen.Cart,
        BottomNavigationScreen.Setting
    )

    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    // if currently user is on bottom bar destination i.e. Home, Post, Cart, Setting screen
    // then only show bottom navigation bar, otherwise don't show it
    val bottomBarDestination = screenList.any {it.route == currentDestination?.route}
    if(bottomBarDestination){
        BottomNavigation(
            backgroundColor = Color.White
        ) {
            screenList.forEach { screen ->
                AddItem(
                    navController = navController,
                    screen = screen,
                    currentDestination = currentDestination
                )
            }
        }
    }

}

@Composable
fun RowScope.AddItem(
    navController: NavHostController,
    screen: BottomNavigationScreen,
    currentDestination: NavDestination?
) {
    val isSelected = currentDestination?.hierarchy?.any { it.route == screen.route } == true
    BottomNavigationItem(
        selected = isSelected,
        icon = {
            Icon(
                imageVector = if (isSelected) screen.selectedIcon else screen.unselectedIcon,
                tint = if (isSelected) Black else TextPlaceholderColor,
                contentDescription = null
            )
        },
        onClick = {
            navController.navigate(screen.route) {
                popUpTo(navController.graph.findStartDestination().id)
                launchSingleTop = true
            }
        }
    )

}


