package com.parvez.superecommerce.ui.composables

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.joelkanyi.jcomposecountrycodepicker.component.KomposeCountryCodePicker
import com.parvez.superecommerce.ui.theme.Black
import com.parvez.superecommerce.ui.theme.TextPlaceholderColor
import com.parvez.superecommerce.ui.theme.White

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TextFieldMobile(
    modifier: Modifier,
    number: String,
    onValueChange: (String) -> Unit
) {

    var isFocused by remember { mutableStateOf(false) }

    OutlinedTextField(
        value = number,
        onValueChange = onValueChange,
        modifier = modifier
            .fillMaxWidth()
            .onFocusChanged {
                isFocused = it.isFocused
            },
        placeholder = { Text(text = "Phone Number") },
        leadingIcon = {
            Row(
                modifier = Modifier.padding(horizontal = 10.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    imageVector = Icons.Default.Phone,
                    contentDescription = null,
                    tint = if (isFocused) Black else TextPlaceholderColor
                )
                KomposeCountryCodePicker(
                    modifier = Modifier,
                    showOnlyCountryCodePicker = true,
                    showCountryFlag = false,
                    colors = TextFieldDefaults.colors(
                        focusedTextColor = Black,
                        unfocusedTextColor = TextPlaceholderColor,
                        focusedLabelColor = Color.Green
                    )
                )
            }

        },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            containerColor = White,
            focusedBorderColor = Black,
            unfocusedBorderColor = TextPlaceholderColor
        ),
        shape = RoundedCornerShape(10.dp),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Phone
        )
    )

}


