package com.parvez.superecommerce.ui.cart.composable

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.parvez.superecommerce.R
import com.parvez.superecommerce.data.model.cart.CartProduct
import com.parvez.superecommerce.ui.theme.TextGray
import com.parvez.superecommerce.ui.theme.TextPlaceholderColor

@Composable
fun CartItem(
    index: Int,
    product: CartProduct,
    showDivider: Boolean
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(150.dp)
            .background(Color.White)
    ) {
        Row(
            modifier = Modifier
                .weight(1f)
                .padding(10.dp),
        ) {

            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(product.title)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(id = R.drawable.placeholder),
                error = painterResource(id = R.drawable.placeholder),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .padding(10.dp)
                    .size(110.dp)
                    .clip(RoundedCornerShape(8.dp))
            )

            Spacer(modifier = Modifier.width(16.dp))

            Column(
                modifier = Modifier.fillMaxSize()
            ) {
                Column(
                    modifier = Modifier
                        .padding(top = 5.dp)
                        .weight(1f)
                ) {
                    Text(
                        text = product.title,
                        style = MaterialTheme.typography.titleMedium
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = "Item #${product.id}",
                        style = MaterialTheme.typography.labelMedium,
                        color = TextPlaceholderColor
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = "$${product.price}",
                        style = MaterialTheme.typography.headlineMedium,
                        fontSize = 16.sp,
                        modifier = Modifier.weight(1f)
                    )
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .clip(RoundedCornerShape(30.dp))
                            .background(Color.LightGray)
                    ) {
                        Image(
                            painterResource(id = R.drawable.ic_minus),
                            contentDescription = null,
                            modifier = Modifier
                                .padding(8.dp)
                                .size(20.dp)
                                .clickable { }
                        )
                        Text(
                            text = product.quantity.toString(),
                            modifier = Modifier
                                .padding(horizontal = 10.dp),
                            fontSize = 14.sp,
                            style = MaterialTheme.typography.headlineLarge,
                            color = TextGray
                        )
                        Image(
                            imageVector = Icons.Default.Add,
                            contentDescription = null,
                            modifier = Modifier
                                .rotate(180f)
                                .padding(8.dp)
                                .size(20.dp)
                                .clickable { }
                        )
                    }

                }
            }
        }
        if (showDivider) {
            Divider(
                color = Color.Gray
            )
        }

    }
}

@Preview
@Composable
fun CartItemPreview() {
    CartItem(
        1,
        CartProduct(
            10.00,
            100,
            1532,
            999,
            10,
            "iPhone 15 pro max",
            11
        ),
        true
    )
}

