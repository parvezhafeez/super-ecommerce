package com.parvez.superecommerce.ui.home.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.paint
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.parvez.superecommerce.R

@Composable
fun CategoryItem(
    index: Int,
    category: String,
    onCategoryClick: (String) -> Unit
) {

    val bg = listOf(R.drawable.bg_1, R.drawable.bg_2, R.drawable.bg_3, R.drawable.bg_4)


    Box(
        modifier = Modifier
            .padding(4.dp)
            .height(150.dp)
            .clip(RoundedCornerShape(10.dp))
            .clickable {
                onCategoryClick(category)
            }
            .paint(
                painter = painterResource(id = bg[index % 4]),
                contentScale = ContentScale.Crop
            ),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = category,
            style = MaterialTheme.typography.titleMedium,
            color = Color.White
        )
    }

}

@Preview
@Composable
fun CategoryItemPreview() {
    CategoryItem(1, "Electronics", {})
}