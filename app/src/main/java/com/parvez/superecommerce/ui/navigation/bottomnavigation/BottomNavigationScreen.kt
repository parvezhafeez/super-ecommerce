package com.parvez.superecommerce.ui.navigation.bottomnavigation

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.List
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material.icons.outlined.ShoppingCart
import androidx.compose.ui.graphics.vector.ImageVector
import com.parvez.superecommerce.R

sealed class BottomNavigationScreen(
    val route: String,
    @StringRes val title: Int,
    val selectedIcon: ImageVector,
    val unselectedIcon: ImageVector
) {

    object Home : BottomNavigationScreen(
        route = "home",
        title = R.string.home,
        selectedIcon = Icons.Default.Home,
        unselectedIcon = Icons.Outlined.Home
    )

    object Post : BottomNavigationScreen(
        route = "post",
        title = R.string.posts,
        selectedIcon = Icons.Default.List,
        unselectedIcon = Icons.Outlined.List
    )

    object Cart : BottomNavigationScreen(
        route = "cart",
        title = R.string.cart,
        selectedIcon = Icons.Default.ShoppingCart,
        unselectedIcon = Icons.Outlined.ShoppingCart
    )

    object Setting : BottomNavigationScreen(
        route = "setting",
        title = R.string.settings,
        selectedIcon = Icons.Default.Settings,
        unselectedIcon = Icons.Outlined.Settings
    )

}

