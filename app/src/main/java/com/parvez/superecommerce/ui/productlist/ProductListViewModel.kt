package com.parvez.superecommerce.ui.productlist

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parvez.superecommerce.data.model.product.ProductListResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.domain.usecase.GetProductListUseCase
import com.parvez.superecommerce.ui.utils.Constants
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val productListUseCase: GetProductListUseCase
) : ViewModel() {

    val category = savedStateHandle.get<String>(Constants.NAV_ARG_CATEGORY_NAME) ?: "null"
    private val _state = mutableStateOf(ProductListUiState())
    val state: State<ProductListUiState>
        get() = _state

    init {
        getProductList(category)
    }

    private fun getProductList(category: String) {
        productListUseCase(category).onEach { result ->
            when (result) {
                is NetworkResponse.Error -> {
                    _state.value = ProductListUiState(error = result.errorMessage)
                }

                is NetworkResponse.Loading -> {
                    _state.value = ProductListUiState(isLoading = true)
                }

                is NetworkResponse.Success -> {
                    _state.value = ProductListUiState(data = result.data)
                }
            }
        }.launchIn(viewModelScope)
    }

}

data class ProductListUiState(
    val isLoading: Boolean = false,
    val data: ProductListResponse? = null,
    val error: String? = ""
)