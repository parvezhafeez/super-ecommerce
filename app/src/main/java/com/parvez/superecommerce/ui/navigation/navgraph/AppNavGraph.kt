package com.parvez.superecommerce.ui.navigation.navgraph

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.parvez.superecommerce.ui.navigation.AppScreen
import com.parvez.superecommerce.ui.productdetails.ProductDetails
import com.parvez.superecommerce.ui.productlist.ProductListScreen

fun NavGraphBuilder.appNavGraph(
    navController: NavHostController
) {

    navigation(
        route = Graph.APP,
        startDestination = AppScreen.ProductList.route
    ) {
        composable(route = AppScreen.ProductList.route) {
            ProductListScreen(navController = navController)
        }

        composable(route = AppScreen.ProductDetail.route) {
            ProductDetails(navController = navController)
        }
    }

}