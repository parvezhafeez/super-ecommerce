package com.parvez.superecommerce.ui.login

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.parvez.superecommerce.R
import com.parvez.superecommerce.ui.composables.GreenFilledButton
import com.parvez.superecommerce.ui.composables.TextFieldEmail
import com.parvez.superecommerce.ui.composables.TextFieldPassword
import com.parvez.superecommerce.ui.login.composable.OrLeftLine
import com.parvez.superecommerce.ui.navigation.AuthScreen
import com.parvez.superecommerce.ui.navigation.navgraph.Graph
import com.parvez.superecommerce.ui.theme.DarkGreen

@Composable
fun LoginScreen(
    navController: NavController,
    loginViewModel: LoginViewModel = hiltViewModel(),
) {

    val state = loginViewModel.state.value

    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }

    if (state.isLoading) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    }

    LaunchedEffect(key1 = state.isLoading) {
        if (state.data != null) {
            navController.popBackStack()
            navController.navigate(route = Graph.HOME)
        }
    }

    // Sample code snippet to retrieve user token
//    LaunchedEffect(key1 = true) {
//        CoroutineScope(Dispatchers.Default).launch {
//            val token = loginViewModel.getUserToken()
//            Log.i("TAG", "Token:: $token")
//        }
//    }


    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 30.dp, vertical = 10.dp)
            .verticalScroll(state = rememberScrollState())
    ) {

        Text(
            text = stringResource(R.string.welcome_back),
            style = MaterialTheme.typography.headlineLarge,
            modifier = Modifier.padding(top = 70.dp)
        )

        Spacer(modifier = Modifier.height(30.dp))

        TextFieldEmail(Modifier, email) { email = it }

        Spacer(modifier = Modifier.height(30.dp))

        TextFieldPassword(Modifier, password) { password = it }

        Spacer(modifier = Modifier.height(30.dp))

        Text(
            text = stringResource(R.string.forgot_password),
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(Alignment.End)
                .clickable {
                    navController.navigate(AuthScreen.ForgotPassword.route)
                },
            style = MaterialTheme.typography.titleMedium,
            color = DarkGreen
        )

        Spacer(modifier = Modifier.height(30.dp))

        GreenFilledButton("Sign In", Modifier, onClick = {
            loginViewModel.login("aeatockj", "szWAG6hc")
        })

        Spacer(modifier = Modifier.height(30.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            OrLeftLine(
                modifier = Modifier
                    .height(2.dp)
                    .width(100.dp)
            )
            Text(
                text = stringResource(R.string.or),
                modifier = Modifier
                    .padding(horizontal = 10.dp)
            )
            OrLeftLine(
                modifier = Modifier
                    .height(2.dp)
                    .width(100.dp)
                    .rotate(180f)
            )
        }

        Spacer(modifier = Modifier.height(30.dp))

        Text(
            text = "Sign in with",
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(align = Alignment.CenterHorizontally),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.titleMedium
        )

        Spacer(modifier = Modifier.height(30.dp))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 50.dp),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_google_circle),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .clip(CircleShape)
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(
                            color = Color.Gray
                        ),
                        onClick = {

                        }
                    )

            )
            Image(
                painter = painterResource(id = R.drawable.ic_apple_circle),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .clip(CircleShape)
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(
                            color = Color.Gray
                        ),
                        onClick = {

                        }
                    )
            )
            Image(
                painter = painterResource(id = R.drawable.ic_fb_circle),
                contentDescription = null,
                modifier = Modifier
                    .size(50.dp)
                    .clip(CircleShape)
                    .clickable(
                        interactionSource = remember { MutableInteractionSource() },
                        indication = rememberRipple(
                            color = Color.Gray
                        ),
                        onClick = {

                        }
                    )
            )
        }

        Spacer(modifier = Modifier.height(30.dp))

        val annotatedString = buildAnnotatedString {
            append(stringResource(R.string.dont_have_an_account))
            withStyle(style = SpanStyle(DarkGreen)) {
                pushStringAnnotation(tag = "signup", annotation = "signup")
                append(" " + stringResource(R.string.sign_up))
            }
        }

        ClickableText(
            text = annotatedString,
            onClick = { offset: Int ->
                annotatedString.getStringAnnotations(offset, offset)
                    .firstOrNull()?.let { span ->
                        when (span.tag) {
                            "signup" -> {
                                // go to signup
                                println("Sign up aayega: ${span.item}")
                                navController.navigate(route = AuthScreen.Signup.route)
                            }
                        }
                    }
            },
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(Alignment.CenterHorizontally),
            style = MaterialTheme.typography.titleMedium

        )

    }
}

@Preview(showSystemUi = true)
@Composable
fun LoginScreenPreview() {
    LoginScreen(rememberNavController())
}

