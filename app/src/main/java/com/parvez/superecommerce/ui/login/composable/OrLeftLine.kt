package com.parvez.superecommerce.ui.login.composable

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.unit.dp

@Composable
fun OrLeftLine(modifier: Modifier) {
    Box(
        modifier = modifier
            .height(2.dp)
            .width(100.dp)
            .background(
                shape = RectangleShape,
                brush = Brush.horizontalGradient(
                    colors = listOf(
                        Color(0xAEAEAE),
                        Color(0xFFaeaeae)
                    )
                )
            )

    )
}