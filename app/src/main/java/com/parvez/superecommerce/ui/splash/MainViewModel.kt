package com.parvez.superecommerce.ui.splash

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parvez.superecommerce.data.model.statuscheck.StatusCheck
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.domain.usecase.GetStatusCheckUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject


@HiltViewModel
class MainViewModel @Inject constructor(
    private val getStatusCheckUseCase: GetStatusCheckUseCase
): ViewModel() {

    private val _state = mutableStateOf(StatusCheckState())
    val state: State<StatusCheckState>
        get() = _state

    init {
        getStatusCheck()
    }

    private fun getStatusCheck(){
        getStatusCheckUseCase().onEach { result: NetworkResponse<StatusCheck> ->
            when(result){
                is NetworkResponse.Error -> {
                    _state.value = StatusCheckState(error = result.errorMessage)
                    println("Inside error")
                }
                is NetworkResponse.Loading -> {
                    _state.value = StatusCheckState(isLoading = true)
                    println("Inside loading")
                }
                is NetworkResponse.Success -> {
                    _state.value = StatusCheckState(data = result.data)
                    println("Inside success")
                }
            }
        }.launchIn(viewModelScope)
    }

}
