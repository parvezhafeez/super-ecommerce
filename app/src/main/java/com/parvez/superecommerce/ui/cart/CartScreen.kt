package com.parvez.superecommerce.ui.cart

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.parvez.superecommerce.data.model.cart.CartProduct
import com.parvez.superecommerce.ui.cart.composable.CartItem
import com.parvez.superecommerce.ui.composables.GreenFilledButton

@Composable
fun CartScreen(
    navController: NavController,
    homeViewModel: CartViewModel = hiltViewModel()
) {

    val state = homeViewModel.state.value

    if (state.isLoading) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator()
        }
    } else {
        Box(
            modifier = Modifier
                .fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            LazyColumn {
                itemsIndexed(
                    state.data?.products ?: emptyList()
                ) { index: Int, product: CartProduct ->
                    val showDivider = index < (state.data?.products?.size!! - 1)
                    CartItem(index, product, showDivider)
                }
                item {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Color.LightGray)
                            .padding(20.dp)
                    ) {
                        Text(
                            text = "Total: \n$${state.data?.total}",
                            modifier = Modifier.weight(1f)
                        )
                        GreenFilledButton(
                            title = "Checkout",
                            modifier = Modifier.width(250.dp)
                        ) {

                        }
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun CartScreenPreview() {
    CartScreen(rememberNavController())
}

