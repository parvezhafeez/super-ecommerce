package com.parvez.superecommerce.ui.post

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parvez.superecommerce.data.model.post.PostResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.domain.usecase.GetPostUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val getPostUseCase: GetPostUseCase
): ViewModel() {

    private val _state = mutableStateOf(PostUiState())
    val state: State<PostUiState>
        get() = _state

    init {
        getPosts()
    }

    private fun getPosts(){
        getPostUseCase.invoke().onEach {result ->
            when(result){
                is NetworkResponse.Error -> {
                    _state.value = PostUiState(error = result.errorMessage)
                }
                is NetworkResponse.Loading -> {
                    _state.value = PostUiState(isLoading = true)
                }
                is NetworkResponse.Success -> {
                    _state.value = PostUiState(data = result.data)
                }
            }
        }.launchIn(viewModelScope)
    }

}

data class PostUiState(
    val isLoading: Boolean = false,
    val data: PostResponse? = null,
    val error: String? = ""
)

