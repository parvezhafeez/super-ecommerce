package com.parvez.superecommerce.ui.theme

import androidx.compose.ui.graphics.Color

//val Purple80 = Color(0xFFD0BCFF)
//val PurpleGrey80 = Color(0xFFCCC2DC)
//val Pink80 = Color(0xFFEFB8C8)

//val Purple40 = Color(0xFF6650a4)
//val PurpleGrey40 = Color(0xFF625b71)
//val Pink40 = Color(0xFF7D5260)

val Black = Color(0xFF000000)
val White = Color(0xFFFFFFFF)
val TextGray = Color(0xFF202020)
val DarkGreen = Color(0xFF008000)
val LightGreen = Color(0xFF00cc00)
val BlueText = Color(0xFF243EE4)
val TextPlaceholderColor = Color(0xFF868585)
val GoldRating = Color(0xFFFAB01B)
