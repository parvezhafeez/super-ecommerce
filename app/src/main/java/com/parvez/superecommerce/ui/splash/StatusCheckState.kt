package com.parvez.superecommerce.ui.splash

import com.parvez.superecommerce.data.model.statuscheck.StatusCheck

data class StatusCheckState(
    val isLoading: Boolean = false,
    val data: StatusCheck? = null,
    val error: String? = ""
)