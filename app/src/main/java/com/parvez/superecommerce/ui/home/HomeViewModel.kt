package com.parvez.superecommerce.ui.home

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parvez.superecommerce.data.model.category.CategoryResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.domain.usecase.GetCategoriesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getCategoriesUseCase: GetCategoriesUseCase
): ViewModel() {

    private val _state = mutableStateOf(HomeUiState())
    val state: State<HomeUiState>
        get() = _state

    init {
        getCategories()
    }

    private fun getCategories() {
        getCategoriesUseCase().onEach {result ->
            when(result){
                is NetworkResponse.Error -> {
                    _state.value = HomeUiState(error = result.errorMessage.toString())
                }
                is NetworkResponse.Loading -> {
                    _state.value = HomeUiState(isLoading = true)
                }
                is NetworkResponse.Success -> {
                    _state.value = HomeUiState(data = result.data)
                }
            }
        }.launchIn(viewModelScope)
    }

}

data class HomeUiState (
    val isLoading: Boolean = false,
    val data: CategoryResponse? = null,
    val error: String? = ""
)