package com.parvez.superecommerce.ui.signup

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.parvez.superecommerce.R
import com.parvez.superecommerce.ui.composables.GreenFilledButton
import com.parvez.superecommerce.ui.composables.TextFieldEmail
import com.parvez.superecommerce.ui.composables.TextFieldName

@Composable
fun SignupScreen(
    navController: NavController
) {

    var firstName by remember { mutableStateOf("") }
    var lastName by remember { mutableStateOf("") }
    var companyName by remember { mutableStateOf("") }
    var email by remember { mutableStateOf("") }
    var mobileNumber by remember { mutableStateOf("") }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(28.dp)
            .verticalScroll(rememberScrollState()),

        ) {
        Image(
            imageVector = Icons.Default.KeyboardArrowLeft,
            contentDescription = null,
            modifier = Modifier
                .clickable {
                    navController.popBackStack()
                }
        )

        Spacer(modifier = Modifier.height(20.dp))

        Text(
            text = stringResource(R.string.create_an_account),
            style = MaterialTheme.typography.headlineLarge
        )

        Spacer(modifier = Modifier.height(20.dp))

        TextFieldName(
            modifier = Modifier,
            name = firstName,
            placeholder = "First Name",
            icon = Icons.Default.Person,
            onValueChange = {
                firstName = it
            }
        )

        Spacer(modifier = Modifier.height(20.dp))

        TextFieldName(
            modifier = Modifier,
            name = lastName,
            placeholder = "Last Name",
            icon = Icons.Default.Person,
            onValueChange = {
                lastName = it
            }
        )

        Spacer(modifier = Modifier.height(20.dp))

        TextFieldName(
            modifier = Modifier,
            name = companyName,
            placeholder = "Company Name (optional)",
            icon = Icons.Filled.Home,
            onValueChange = {
                companyName = it
            }
        )

        Spacer(modifier = Modifier.height(20.dp))

        TextFieldEmail(
            modifier = Modifier,
            email = email,
            onValueChange = {
                email = it
            }
        )

        Spacer(modifier = Modifier.height(20.dp))

        TextFieldName(
            modifier = Modifier,
            name = mobileNumber,
            placeholder = "Mobile Number",
            icon = Icons.Default.Phone,
            keyboardType = KeyboardType.Phone,
            onValueChange = {
                mobileNumber = it
            }
        )

        Spacer(modifier = Modifier.height(30.dp))

        GreenFilledButton(
            title = stringResource(R.string.submit),
            modifier = Modifier
        ) {

        }

    }
}

@Preview(showBackground = true)
@Composable
fun SignupScreenPreview() {
    SignupScreen(navController = rememberNavController())
}
