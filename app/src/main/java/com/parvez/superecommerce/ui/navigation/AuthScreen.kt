package com.parvez.superecommerce.ui.navigation

sealed class AuthScreen(val route: String) {

    object Onboarding : AuthScreen("onboarding")

    object Login : AuthScreen("login")

    object Signup : AuthScreen("signup")

    object ForgotPassword : AuthScreen("forgot_password")

    object OtpVerification : AuthScreen("otp_verification")

}


