package com.parvez.superecommerce.ui.navigation.navgraph

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.parvez.superecommerce.ui.forgotpassword.ForgotPasswordScreen
import com.parvez.superecommerce.ui.login.LoginScreen
import com.parvez.superecommerce.ui.navigation.AuthScreen
import com.parvez.superecommerce.ui.onboarding.OnBoarding
import com.parvez.superecommerce.ui.otpverification.OtpVerificationScreen
import com.parvez.superecommerce.ui.signup.SignupScreen

fun NavGraphBuilder.authNavGraph(
    navController: NavHostController
) {
    navigation(
        route = Graph.AUTH,
        startDestination = AuthScreen.Onboarding.route
    ) {
        composable(
            route = AuthScreen.Onboarding.route
        ) {
            OnBoarding(navController = navController)
        }

        composable(
            route = AuthScreen.Login.route
        ) {
            LoginScreen(navController = navController)
        }

        composable(
            route = AuthScreen.Signup.route
        ) {
            SignupScreen(navController = navController)
        }

        composable(
            route = AuthScreen.ForgotPassword.route
        ) {
            ForgotPasswordScreen(navController = navController)
        }

        composable(
            route = AuthScreen.OtpVerification.route
        ) {
            OtpVerificationScreen(navController = navController)
        }
    }
}

