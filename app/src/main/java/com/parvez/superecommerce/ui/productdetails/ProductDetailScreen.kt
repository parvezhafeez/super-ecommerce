package com.parvez.superecommerce.ui.productdetails

import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.outlined.ShoppingCart
import androidx.compose.material3.Badge
import androidx.compose.material3.BadgedBox
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.parvez.superecommerce.R
import com.parvez.superecommerce.ui.composables.GreenFilledButton
import com.parvez.superecommerce.ui.theme.DarkGreen
import com.parvez.superecommerce.ui.theme.GoldRating
import com.parvez.superecommerce.ui.theme.LightGreen
import com.parvez.superecommerce.ui.theme.TextGray

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductDetails(
    navController: NavController,
    productDetailViewModel: ProductDetailViewModel = hiltViewModel()
) {

    val state = productDetailViewModel.state.value
    val addToCartState = productDetailViewModel.addToCartState.value

    val context = LocalContext.current
    if (!addToCartState.isLoading && addToCartState.data != null) {
        Toast.makeText(context, "Product Added Successfully!", Toast.LENGTH_SHORT).show()
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = state.data?.title ?: "") },
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Default.KeyboardArrowLeft,
                            contentDescription = null
                        )
                    }
                },
                actions = {
                    BadgedBox(
                        badge = {
                            Badge(
                                containerColor = LightGreen
                            ) {
                                val badgeNumber = "0"
                                Text(text = badgeNumber)
                            }
                        },
                        modifier = Modifier.padding(end = 16.dp)
                    ) {
                        Icon(
                            imageVector = Icons.Outlined.ShoppingCart,
                            contentDescription = null,
                            modifier = Modifier.clickable {

                            }
                        )
                    }
                }
            )
        }
    ) {
        Box(
            modifier = Modifier
                .padding(it)
                .fillMaxWidth()
        ) {
            Column(
                modifier = Modifier.fillMaxSize()
            ) {
                Column(
                    modifier = Modifier
                        .weight(1f)
                ) {
                    AsyncImage(
                        model = ImageRequest.Builder(LocalContext.current)
                            .data(state.data?.thumbnail)
                            .crossfade(true)
                            .build(),
                        placeholder = painterResource(id = R.drawable.placeholder),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .padding(10.dp)
                            .fillMaxWidth()
                            .height(200.dp)
                            .shadow(elevation = 6.dp, shape = RoundedCornerShape(8.dp))
                            .clip(RoundedCornerShape(8.dp))

                    )

                    Text(
                        text = state.data?.description ?: "NA",
                        style = MaterialTheme.typography.titleMedium,
                        color = TextGray,
                        modifier = Modifier.padding(start = 10.dp, top = 10.dp, end = 10.dp)
                    )

                    Row(
                        modifier = Modifier
                            .padding(start = 10.dp, top = 10.dp, end = 10.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = "$${state.data?.price.toString()}",
                            style = MaterialTheme.typography.headlineMedium,
                            color = DarkGreen
                        )

                        Spacer(modifier = Modifier.width(20.dp))

                        Icon(
                            imageVector = Icons.Default.Star,
                            contentDescription = null,
                            tint = GoldRating
                        )
                        Text(
                            text = "$${state.data?.rating.toString()}",
                            style = MaterialTheme.typography.headlineMedium,
                            color = TextGray,

                            )
                    }
                }

                GreenFilledButton(
                    title = "Add To Cart",
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp, vertical = 10.dp)
                ) {
                    productDetailViewModel.addProductToCart(state.data!!)
                }

            }

            if (state.isLoading || addToCartState.isLoading) {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    CircularProgressIndicator()
                }
            }
        }
    }

}

@Preview(showBackground = true)
@Composable
fun ProductDetailsPreview() {
    ProductDetails(rememberNavController())
}
