package com.parvez.superecommerce.ui.navigation

import com.parvez.superecommerce.ui.utils.Constants

sealed class AppScreen(val route: String) {

    object ProductList : AppScreen("product_list/{${Constants.NAV_ARG_CATEGORY_NAME}}") {
        fun withCategoryName(category: String): String {
            return "product_list/$category"
        }
    }

    object ProductDetail : AppScreen("product_detail/{${Constants.NAV_ARG_PRODUCT_ID}}"){
        fun withProductId(productId: String): String{
            return "product_detail/$productId"
        }
    }

}
