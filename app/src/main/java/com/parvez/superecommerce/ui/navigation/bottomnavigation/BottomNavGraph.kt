package com.parvez.superecommerce.ui.navigation.bottomnavigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.parvez.superecommerce.ui.cart.CartScreen
import com.parvez.superecommerce.ui.home.HomeScreen
import com.parvez.superecommerce.ui.navigation.navgraph.Graph
import com.parvez.superecommerce.ui.navigation.navgraph.appNavGraph
import com.parvez.superecommerce.ui.post.PostScreen
import com.parvez.superecommerce.ui.settings.SettingScreen

@Composable
fun BottomNavGraph(
    navController: NavHostController
) {

    NavHost(
        route = Graph.HOME,
        startDestination = BottomNavigationScreen.Home.route,
        navController = navController
    ) {

        composable(route = BottomNavigationScreen.Home.route) {
            HomeScreen(navController = navController)
        }

        composable(route = BottomNavigationScreen.Post.route) {
            PostScreen()
        }

        composable(route = BottomNavigationScreen.Cart.route) {
            CartScreen(navController = navController)
        }

        composable(route = BottomNavigationScreen.Setting.route) {
            SettingScreen()
        }

        appNavGraph(navController = navController)

    }

}
