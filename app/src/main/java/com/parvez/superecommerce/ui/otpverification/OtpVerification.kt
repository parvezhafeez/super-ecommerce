package com.parvez.superecommerce.ui.otpverification

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.parvez.superecommerce.R
import com.parvez.superecommerce.ui.composables.GreenFilledButton
import com.parvez.superecommerce.ui.composables.TextFieldOtp
import com.parvez.superecommerce.ui.theme.DarkGreen

@Composable
fun OtpVerificationScreen(
    navController: NavController
) {

    var otpValue by remember { mutableStateOf("") }
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(28.dp)
            .verticalScroll(rememberScrollState()),
    ) {

        Image(
            imageVector = Icons.Default.KeyboardArrowLeft,
            contentDescription = null,
            modifier = Modifier
                .clickable {
                    navController.popBackStack()
                }
        )

        Spacer(modifier = Modifier.height(20.dp))

        Text(
            text = stringResource(R.string.otp_verification),
            style = MaterialTheme.typography.headlineLarge,
        )

        Spacer(modifier = Modifier.height(20.dp))

        TextFieldOtp(
            modifier = Modifier,
            otpText = otpValue,
            onOtpTextChange = { value ->
                otpValue = value
            }
        )

        Spacer(modifier = Modifier.height(20.dp))

        GreenFilledButton(
            title = stringResource(R.string.verify),
            modifier = Modifier
        ) {
            validateOtp(otpValue)
        }

        Spacer(modifier = Modifier.height(20.dp))

        Text(
            text = "Resend OTP",
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(align = Alignment.CenterHorizontally),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.titleMedium,
            color = DarkGreen
        )
    }

}

private fun validateOtp(otpValue: String) {
    otpValue?.isEmpty()
}

@Preview(showBackground = true)
@Composable
fun OtpVerificationScreenPreview() {
    OtpVerificationScreen(rememberNavController())
}

