package com.parvez.superecommerce.ui.login

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parvez.superecommerce.data.model.login.LoginResponse
import com.parvez.superecommerce.data.prefdatastore.DataStoreManager
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.domain.usecase.LoginUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase,
    private val dataStoreManager: DataStoreManager
) : ViewModel() {


    private val _state = mutableStateOf(LoginUiState())
    val state: State<LoginUiState>
        get() = _state


    fun login(username: String, password: String) {
        loginUseCase(username, password).onEach { result: NetworkResponse<LoginResponse> ->
            when (result) {
                is NetworkResponse.Error -> {
                    _state.value = LoginUiState(error = result.errorMessage)
                }

                is NetworkResponse.Loading -> {
                    _state.value = LoginUiState(isLoading = true)
                }

                is NetworkResponse.Success -> {
                    _state.value = LoginUiState(data = result.data)
                    saveUserToken(result.data?.token.toString())
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun saveUserToken(userToken: String) {
        viewModelScope.launch {
            dataStoreManager.saveUserTokenToDataStore(userToken)
        }
    }

    suspend fun getUserToken(): String {
        val token: Deferred<String> = viewModelScope.async {
            dataStoreManager.getUserTokenFromDataStore()
        }
        return token.await()
    }


}

data class LoginUiState(
    val isLoading: Boolean = false,
    val data: LoginResponse? = null,
    val error: String? = ""
)
