package com.parvez.superecommerce.ui.composables

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.parvez.superecommerce.ui.theme.DarkGreen
import com.parvez.superecommerce.ui.theme.White

@Composable
fun GreenFilledButton(
    title: String,
    modifier: Modifier,
    onClick: ()-> Unit
) {
    Button(
        onClick = onClick,
        modifier = modifier
            .fillMaxWidth()
            .height(48.dp),
        shape = RoundedCornerShape(10.dp),
        colors = ButtonDefaults.buttonColors(
            containerColor = DarkGreen,
            contentColor = White
        )
    ) {
        Text(
            text = title,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 18.sp,
            color = White
        )
    }
}

@Preview
@Composable
fun GreenFilledButtonPreview() {
    GreenFilledButton(title = "Sign In", modifier = Modifier) { }
}