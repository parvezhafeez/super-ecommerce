package com.parvez.superecommerce.ui.post.composable

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.parvez.superecommerce.R
import com.parvez.superecommerce.data.model.post.Post
import com.parvez.superecommerce.ui.theme.TextPlaceholderColor

@Composable
fun PostItem(
    post: Post
) {

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = Color.White)
    ) {

        Image(
            painter = painterResource(id = R.drawable.ic_launcher_background),
            contentDescription = "Post image",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .padding(10.dp)
                .size(60.dp)
                .clip(CircleShape)
        )

        Column(
            modifier = Modifier
                .weight(1f)
                .padding(10.dp)
        ) {
            Text(
                text = post.title,
                style = MaterialTheme.typography.titleMedium
            )
            Text(
                text = post.body,
                style = MaterialTheme.typography.labelMedium,
                color = TextPlaceholderColor,
                maxLines = 4,
                overflow = TextOverflow.Ellipsis
            )
            Row(
                modifier = Modifier
                    .padding(top = 10.dp)
            ) {
                Image(
                    imageVector = Icons.Default.ThumbUp,
                    contentDescription = null,
                    colorFilter = ColorFilter.tint(color = TextPlaceholderColor),
                    modifier = Modifier
                        .clickable {

                        }
                )
                Spacer(modifier = Modifier.width(6.dp))
                Text(text = post.reactions.toString())
            }
            Divider(
                Modifier.padding(top = 10.dp)
            )
        }


    }


}

@Preview
@Composable
fun PostItemPreview() {
    PostItem(
        Post(
            id = 1,
            title = "Test",
            body = "Body",
            userId = 9,
            reactions = 3,
            tags = emptyList()
        )
    )
}
