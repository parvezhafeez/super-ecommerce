package com.parvez.superecommerce.ui.post

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import com.parvez.superecommerce.ui.post.composable.PostItem

@Composable
fun PostScreen(
    postViewModel: ProductViewModel = hiltViewModel()
) {
    
    val state = postViewModel.state.value
    
    Box(
        modifier = Modifier
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        
        LazyColumn{
            items(state.data?.posts?: emptyList()){ post ->
                PostItem(post = post)
            }
        }
        
    }
}

@Preview
@Composable
fun PostScreenPreview() {
    PostScreen()
}