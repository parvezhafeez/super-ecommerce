package com.parvez.superecommerce.ui.cart

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parvez.superecommerce.data.model.cart.CartResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.domain.usecase.GetCartUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val getCartUseCase: GetCartUseCase
) : ViewModel() {

    private val _state = mutableStateOf(CartUiState())
    val state: State<CartUiState>
        get() = _state

    init {
        getCart()
    }

    private fun getCart() {
        getCartUseCase.invoke().onEach { result ->
            when (result) {
                is NetworkResponse.Error -> {
                    _state.value = CartUiState(error = result.errorMessage)
                }

                is NetworkResponse.Loading -> {
                    _state.value = CartUiState(isLoading = true)
                }

                is NetworkResponse.Success -> {
                    Log.i("TAG", "Success, Size: ${result.data?.products?.size}")
                    _state.value = CartUiState(data = result.data)
                }
            }
        }.launchIn(viewModelScope)
    }

}

data class CartUiState(
    val isLoading: Boolean = false,
    val data: CartResponse? = null,
    val error: String? = ""
)