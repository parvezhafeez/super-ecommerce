package com.parvez.superecommerce.ui.composables

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.parvez.superecommerce.ui.theme.Black
import com.parvez.superecommerce.ui.theme.TextPlaceholderColor
import com.parvez.superecommerce.ui.theme.White

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TextFieldName(
    modifier: Modifier,
    name: String,
    placeholder: String,
    icon: ImageVector,
    keyboardType: KeyboardType = KeyboardType.Text,
    onValueChange: (String) -> Unit
) {

    var isFocused by remember { mutableStateOf(false) }

    OutlinedTextField(
        value = name,
        onValueChange = onValueChange,
        modifier = modifier
            .fillMaxWidth()
            .onFocusChanged {
                isFocused = it.isFocused
            },
        singleLine = true,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            containerColor = White,
            focusedBorderColor = Black,
            unfocusedBorderColor = TextPlaceholderColor
        ),
        shape = RoundedCornerShape(10.dp),
        placeholder = {
            Text(
                text = placeholder,
                style = MaterialTheme.typography.labelMedium
            )
        },
        textStyle = MaterialTheme.typography.labelLarge,
        leadingIcon = {
            Icon(
                imageVector = icon,
                contentDescription = null,
                tint = if (isFocused) Black else TextPlaceholderColor
            )
        },
        keyboardOptions = KeyboardOptions(
            keyboardType = keyboardType,
            capitalization = KeyboardCapitalization.Words
        )
    )

}

@Preview(showBackground = true)
@Composable
fun TextFieldNamePreview() {
    TextFieldName(
        modifier = Modifier,
        name = "",
        placeholder = "Enter Name",
        icon = Icons.Default.Person,
        onValueChange = {}
    )
}