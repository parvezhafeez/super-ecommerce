package com.parvez.superecommerce.ui.onboarding

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState
import com.parvez.superecommerce.ui.navigation.AuthScreen
import com.parvez.superecommerce.ui.onboarding.composable.BottomSection
import com.parvez.superecommerce.ui.onboarding.composable.OnBoardingItem
import com.parvez.superecommerce.ui.onboarding.composable.TopSection
import com.parvez.superecommerce.ui.onboarding.model.OnBoardingItems
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@OptIn(ExperimentalPagerApi::class)
@Composable
fun OnBoarding(
    navController: NavController
) {
    val context = LocalContext.current
    val items: List<OnBoardingItems> = OnBoardingItems.getData()
    val scope: CoroutineScope = rememberCoroutineScope()
    val pageState: PagerState = rememberPagerState()

    Column(modifier = Modifier.fillMaxSize()) {

        TopSection(
            onBackClick = {
                if (pageState.currentPage + 1 > 1) scope.launch {
                    pageState.scrollToPage(pageState.currentPage - 1)
                }
            },
            onSkipClick = {
                if (pageState.currentPage + 1 < items.size) scope.launch {
                    pageState.scrollToPage(items.size - 1)
                }
            },
            pageState.currentPage < items.size - 1
        )


        HorizontalPager(
            count = items.size,
            state = pageState,
            modifier = Modifier
                .fillMaxHeight(0.9f)
                .fillMaxWidth()
        ) { page ->
            OnBoardingItem(item = items[page])
        }


        BottomSection(size = items.size, index = pageState.currentPage) {
            if (pageState.currentPage + 1 < items.size) {
                scope.launch {
                    pageState.scrollToPage(pageState.currentPage + 1)
                }
            } else {
                navController.navigate(route = AuthScreen.Login.route) {
                    popUpTo(route = AuthScreen.Onboarding.route) {
                        inclusive = true
                    }
                }
            }
        }

    }

}


