package com.parvez.superecommerce.ui.productlist.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.parvez.superecommerce.R
import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.ui.theme.BlueText

@Composable
fun ProductListItem(
    product: Product,
    onItemClick: (Product) -> Unit
) {

    val context = LocalContext.current

    Card(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 8.dp
        ),
        modifier = Modifier
            .padding(10.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    onItemClick(product)
                }
        ) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(product.thumbnail)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(id = R.drawable.placeholder),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .padding(10.dp)
                    .size(110.dp)
                    .clip(RoundedCornerShape(8.dp))
            )

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp),
            ) {
                Text(
                    text = product.title,
                    style = MaterialTheme.typography.headlineSmall
                )

                Spacer(modifier = Modifier.height(10.dp))

                Text(
                    text = "Brand: ${product.brand}",
                    fontSize = 12.sp
                )

                Spacer(modifier = Modifier.height(10.dp))

                Text(
                    text = "$${product.price}",
                    style = MaterialTheme.typography.labelMedium,
                    color = BlueText,
                    fontSize = 18.sp
                )

            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ProductListItemPreview() {
    ProductListItem(
        Product(
            id = 1,
            title = "iPhone 15 Pro",
            description = "An apple mobile which is nothing like apple",
            price = 549,
            discountPercentage = 12.96,
            rating = 4.0,
            stock = 98,
            brand = "Apple",
            category = "smartphones",
            thumbnail = "https://cdn.vox-cdn.com/thumbor/avUh8dza7ognlbD9brA0nMI6ykI=/0x0:2080x1169/2080x1169/filters:focal(1040x585:1041x586)/cdn.vox-cdn.com/uploads/chorus_asset/file/24916101/Screenshot_2023_09_12_at_1.19.32_PM.png",
            images = emptyList()
        )
    ){}
}