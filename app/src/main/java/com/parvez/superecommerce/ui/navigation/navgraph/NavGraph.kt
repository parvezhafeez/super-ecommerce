package com.parvez.superecommerce.ui.navigation.navgraph

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.parvez.superecommerce.ui.navigation.bottomnavigation.MainScreen

@Composable
fun SetupNavGraph(
    navController: NavHostController
) {
    NavHost(
        route = Graph.ROOT,
        startDestination = Graph.AUTH,
        navController = navController
    ) {
        authNavGraph(navController = navController)

        composable(route = Graph.HOME) {
            MainScreen()
        }
    }
}

object Graph {
    const val ROOT = "root_graph"
    const val AUTH = "auth_graph"
    const val HOME = "home_graph"
    const val APP = "app_graph"
}