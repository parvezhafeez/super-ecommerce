package com.parvez.superecommerce.ui.utils

object Constants {

//    const val ROOT_ROUTE = "root_route"
//    const val AUTH_ROUTE = "auth_route"

    const val NAV_ARG_CATEGORY_NAME = "category_name"
    const val NAV_ARG_PRODUCT_ID = "product_id"

}