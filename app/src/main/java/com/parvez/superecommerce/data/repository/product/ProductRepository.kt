package com.parvez.superecommerce.data.repository.product

import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.model.product.ProductListResponse
import com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource
import javax.inject.Inject

class ProductRepository @Inject constructor(
    private val productRemoteSource: ProductRemoteSource
) {

    suspend fun getProducts(category: String): ProductListResponse {
        return productRemoteSource.getProducts(category)
    }

    suspend fun getProduct(productId: String): Product {
        return productRemoteSource.getProduct(productId)
    }

}