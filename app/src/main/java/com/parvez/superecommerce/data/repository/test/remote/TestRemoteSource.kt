package com.parvez.superecommerce.data.repository.test.remote

import com.parvez.superecommerce.data.model.statuscheck.StatusCheck

interface TestRemoteSource {

    suspend fun test(): StatusCheck

}