package com.parvez.superecommerce.data.repository.post

import com.parvez.superecommerce.data.model.post.PostResponse
import com.parvez.superecommerce.data.repository.post.remote.PostRemoteSource
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val postRemoteSource: PostRemoteSource
) {

    suspend fun getPost(): PostResponse{
        return postRemoteSource.getPosts()
    }

}
