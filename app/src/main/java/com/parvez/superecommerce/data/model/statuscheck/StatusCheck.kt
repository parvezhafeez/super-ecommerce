package com.parvez.superecommerce.data.model.statuscheck

data class StatusCheck(
    val method: String,
    val status: String
)