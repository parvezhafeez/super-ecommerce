package com.parvez.superecommerce.data.repository.login.remote

import com.parvez.superecommerce.data.model.login.LoginResponse

interface LoginRemoteSource {

    suspend fun login(username: String, password: String): LoginResponse

}