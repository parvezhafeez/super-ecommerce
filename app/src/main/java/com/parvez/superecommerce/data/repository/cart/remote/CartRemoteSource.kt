package com.parvez.superecommerce.data.repository.cart.remote

import com.parvez.superecommerce.data.model.cart.CartResponse
import com.parvez.superecommerce.data.model.product.Product

interface CartRemoteSource {

    suspend fun addProductToCart(product: Product): Product

    suspend fun getCart(): CartResponse

}