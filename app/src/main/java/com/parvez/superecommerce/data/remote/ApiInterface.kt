package com.parvez.superecommerce.data.remote

import com.parvez.superecommerce.data.model.cart.CartResponse
import com.parvez.superecommerce.data.model.category.CategoryResponse
import com.parvez.superecommerce.data.model.login.LoginResponse
import com.parvez.superecommerce.data.model.post.PostResponse
import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.model.product.ProductListResponse
import com.parvez.superecommerce.data.model.statuscheck.StatusCheck
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiInterface {

    @GET("/test")
    suspend fun test(): StatusCheck

    @FormUrlEncoded
    @POST("/auth/login")
    suspend fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): LoginResponse

    @GET("/products/categories")
    suspend fun getCategories(): CategoryResponse

    @GET("/products/category/{category}")
    suspend fun getProducts(
        @Path("category") category: String
    ): ProductListResponse

    @GET("/products/{productId}")
    suspend fun getProduct(
        @Path("productId") productId: String
    ): Product

    @POST("/products/add")
    suspend fun addProductToCart(
        @Body product: Product
    ): Product

    @GET("/carts/1")
    suspend fun getCart(): CartResponse

    @GET("/posts")
    suspend fun getPosts(): PostResponse
}