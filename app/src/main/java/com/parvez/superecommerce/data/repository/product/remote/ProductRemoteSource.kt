package com.parvez.superecommerce.data.repository.product.remote

import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.model.product.ProductListResponse

interface ProductRemoteSource {

    suspend fun getProducts(category: String): ProductListResponse

    suspend fun getProduct(productId: String): Product

}