package com.parvez.superecommerce.data.model.product

data class ProductListResponse(
    val limit: Int,
    val products: List<Product>,
    val skip: Int,
    val total: Int
)