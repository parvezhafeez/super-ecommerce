package com.parvez.superecommerce.data.prefdatastore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class DataStoreManager @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    companion object {
        val PREF_USER_TOKEN = stringPreferencesKey("USER_TOKEN")
    }

    suspend fun saveUserTokenToDataStore(value: String) {
        dataStore.edit {
            it[PREF_USER_TOKEN] = value
        }
    }

    suspend fun getUserTokenFromDataStore(): String {
        val pref = dataStore.data.first()
        return pref[PREF_USER_TOKEN]!!
    }

    suspend fun clearDataStore() = dataStore.edit {
        it.clear()
    }
}
