package com.parvez.superecommerce.data.repository.cart

import com.parvez.superecommerce.data.model.cart.CartResponse
import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource
import javax.inject.Inject

class CartRepository @Inject constructor(
    private val cartRemoteSource: CartRemoteSource
) {

    suspend fun addProductToCart(product: Product): Product {
        return cartRemoteSource.addProductToCart(product)
    }

    suspend fun getCart(): CartResponse {
        return cartRemoteSource.getCart()
    }
}
