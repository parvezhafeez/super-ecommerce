package com.parvez.superecommerce.data.repository.category.remote

import com.parvez.superecommerce.data.model.category.CategoryResponse

interface CategoryRemoteSource {

    suspend fun getCategories(): CategoryResponse

}