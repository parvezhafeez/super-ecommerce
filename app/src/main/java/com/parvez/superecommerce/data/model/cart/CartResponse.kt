package com.parvez.superecommerce.data.model.cart

data class CartResponse(
    val discountedTotal: Int,
    val id: Int,
    val products: List<CartProduct>,
    val total: Int,
    val totalProducts: Int,
    val totalQuantity: Int,
    val userId: Int
)