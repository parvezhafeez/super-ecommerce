package com.parvez.superecommerce.data.repository.test.remote

import com.parvez.superecommerce.data.model.statuscheck.StatusCheck
import com.parvez.superecommerce.data.remote.ApiInterface
import javax.inject.Inject

class TestRemoteSourceImpl @Inject constructor(
    private val apiInterface: ApiInterface
): TestRemoteSource {

    override suspend fun test(): StatusCheck {
        return apiInterface.test()
    }

}