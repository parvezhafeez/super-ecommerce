package com.parvez.superecommerce.data.repository.test

import com.parvez.superecommerce.data.model.statuscheck.StatusCheck
import com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource
import javax.inject.Inject

class TestRepository @Inject constructor(
    private val testRemoteSource: TestRemoteSource
) {

    suspend fun test(): StatusCheck {
        return testRemoteSource.test()
    }

}