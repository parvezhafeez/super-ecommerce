package com.parvez.superecommerce.data.repository.category

import com.parvez.superecommerce.data.model.category.CategoryResponse
import com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSource
import javax.inject.Inject

class CategoryRepository @Inject constructor(
    private val categoryRemoteSource: CategoryRemoteSource
) {

    suspend fun getCategories(): CategoryResponse{
        return categoryRemoteSource.getCategories()
    }

}
