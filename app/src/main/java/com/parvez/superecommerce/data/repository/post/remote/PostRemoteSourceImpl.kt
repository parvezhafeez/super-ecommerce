package com.parvez.superecommerce.data.repository.post.remote

import com.parvez.superecommerce.data.model.post.PostResponse
import com.parvez.superecommerce.data.remote.ApiInterface
import javax.inject.Inject

class PostRemoteSourceImpl @Inject constructor(
    private val apiInterface: ApiInterface
) : PostRemoteSource {
    override suspend fun getPosts(): PostResponse {
        return apiInterface.getPosts()
    }
}