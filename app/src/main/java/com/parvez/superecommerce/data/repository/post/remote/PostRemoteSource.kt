package com.parvez.superecommerce.data.repository.post.remote

import com.parvez.superecommerce.data.model.post.PostResponse

interface PostRemoteSource {

    suspend fun getPosts(): PostResponse

}