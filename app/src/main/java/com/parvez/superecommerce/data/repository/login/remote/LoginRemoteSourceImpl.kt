package com.parvez.superecommerce.data.repository.login.remote

import com.parvez.superecommerce.data.model.login.LoginResponse
import com.parvez.superecommerce.data.remote.ApiInterface
import javax.inject.Inject

class LoginRemoteSourceImpl @Inject constructor(
    private val apiInterface: ApiInterface
): LoginRemoteSource {

    override suspend fun login(username: String, password: String): LoginResponse {
        return apiInterface.login(username, password)
    }
}

