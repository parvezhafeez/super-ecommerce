package com.parvez.superecommerce.data.repository.cart.remote

import com.parvez.superecommerce.data.model.cart.CartResponse
import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.remote.ApiInterface
import javax.inject.Inject

class CartRemoteSourceImpl @Inject constructor(
    private val apiInterface: ApiInterface
): CartRemoteSource {
    override suspend fun addProductToCart(product: Product): Product {
        return apiInterface.addProductToCart(product)
    }

    override suspend fun getCart(): CartResponse {
        return apiInterface.getCart()
    }
}
