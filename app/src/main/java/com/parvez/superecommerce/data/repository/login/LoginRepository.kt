package com.parvez.superecommerce.data.repository.login

import com.parvez.superecommerce.data.model.login.LoginResponse
import com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSource
import javax.inject.Inject

class LoginRepository @Inject constructor(
    private val loginRemoteSource: LoginRemoteSource
) {

    suspend fun login(username: String, password: String): LoginResponse{
        return loginRemoteSource.login(username, password)
    }

}
