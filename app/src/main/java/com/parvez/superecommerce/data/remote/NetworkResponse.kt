package com.parvez.superecommerce.data.remote

sealed class NetworkResponse<T>(
    val data: T? = null,
    val errorMessage: String? = null
){
    class Loading<T>(): NetworkResponse<T>()
    class Success<T>(data: T): NetworkResponse<T>(data = data)
    class Error<T>(errorMsg: String, data: T? = null): NetworkResponse<T>(errorMessage = errorMsg, data = data)
}
