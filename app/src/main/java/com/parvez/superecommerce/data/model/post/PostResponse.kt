package com.parvez.superecommerce.data.model.post

data class PostResponse(
    val limit: Int,
    val posts: List<Post>,
    val skip: Int,
    val total: Int
)