package com.parvez.superecommerce.data.repository.category.remote

import com.parvez.superecommerce.data.model.category.CategoryResponse
import com.parvez.superecommerce.data.remote.ApiInterface
import javax.inject.Inject

class CategoryRemoteSourceImpl @Inject constructor(
    private val apiInterface: ApiInterface
): CategoryRemoteSource {
    override suspend fun getCategories(): CategoryResponse {
        return apiInterface.getCategories()
    }
}