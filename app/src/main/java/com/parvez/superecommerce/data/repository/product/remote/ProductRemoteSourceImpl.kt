package com.parvez.superecommerce.data.repository.product.remote

import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.model.product.ProductListResponse
import com.parvez.superecommerce.data.remote.ApiInterface
import javax.inject.Inject

class ProductRemoteSourceImpl @Inject constructor(
    private val apiInterface: ApiInterface
): ProductRemoteSource {
    override suspend fun getProducts(category: String): ProductListResponse {
        return apiInterface.getProducts(category)
    }

    override suspend fun getProduct(productId: String): Product {
        return apiInterface.getProduct(productId)
    }
}