package com.parvez.superecommerce.domain.usecase

import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.data.repository.cart.CartRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class AddProductToCartUseCase @Inject constructor(
    private val cartRepository: CartRepository
) {

    operator fun invoke(product: Product): Flow<NetworkResponse<Product>> = flow {
        try {
            emit(NetworkResponse.Loading())
            val response = cartRepository.addProductToCart(product)
            emit(NetworkResponse.Success(data = response))
        } catch (e: HttpException) {
            emit(NetworkResponse.Error(errorMsg = e.localizedMessage ?: "Unexpected error !"))
        } catch (e: IOException) {
            emit(NetworkResponse.Error(errorMsg = "Please check your internet connection"))
        }
    }

}
