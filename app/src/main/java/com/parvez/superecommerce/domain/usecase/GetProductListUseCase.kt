package com.parvez.superecommerce.domain.usecase

import com.parvez.superecommerce.data.model.product.ProductListResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.data.repository.product.ProductRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetProductListUseCase @Inject constructor(
    private val productRepository: ProductRepository
) {

    operator fun invoke(category: String): Flow<NetworkResponse<ProductListResponse>> = flow {
        try {
            emit(NetworkResponse.Loading())
            val response = productRepository.getProducts(category)
            emit(NetworkResponse.Success(data = response))
        } catch (e: HttpException) {
            emit(NetworkResponse.Error(errorMsg = e.localizedMessage ?: "Unexpected error !"))
        } catch (e: IOException) {
            emit(NetworkResponse.Error(errorMsg = "Please check your internet connection"))
        }
    }

}