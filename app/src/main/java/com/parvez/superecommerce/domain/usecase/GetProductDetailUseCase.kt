package com.parvez.superecommerce.domain.usecase

import com.parvez.superecommerce.data.model.product.Product
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.data.repository.product.ProductRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetProductDetailUseCase @Inject constructor(
    private val productRepository: ProductRepository
) {

    operator fun invoke(productId: String): Flow<NetworkResponse<Product>> = flow {
        try {
            emit(NetworkResponse.Loading())
            val response = productRepository.getProduct(productId)
            emit(NetworkResponse.Success(data = response))
        } catch (e: HttpException) {
            emit(NetworkResponse.Error(errorMsg = e.localizedMessage ?: "Unexpected error !"))
        } catch (e: IOException) {
            emit(NetworkResponse.Error(errorMsg = "Please check your internet connection"))
        }
    }

}