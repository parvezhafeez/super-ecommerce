package com.parvez.superecommerce.domain.usecase

import com.parvez.superecommerce.data.model.statuscheck.StatusCheck
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.data.repository.test.TestRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetStatusCheckUseCase @Inject constructor(
    private val testRepository: TestRepository
) {

    operator fun invoke(): Flow<NetworkResponse<StatusCheck>> = flow {
        try {
            emit(NetworkResponse.Loading())
            val response: StatusCheck = testRepository.test()
            emit(NetworkResponse.Success(data = response))
        }catch (e: HttpException){
            emit(NetworkResponse.Error(errorMsg = e.localizedMessage?: "Unexpected error !"))
        }catch (e: IOException){
            emit(NetworkResponse.Error(errorMsg = "Please check your internet connection"))
        }

    }

}