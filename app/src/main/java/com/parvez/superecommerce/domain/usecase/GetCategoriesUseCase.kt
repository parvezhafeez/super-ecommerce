package com.parvez.superecommerce.domain.usecase

import com.parvez.superecommerce.data.model.category.CategoryResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.data.repository.category.CategoryRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetCategoriesUseCase @Inject constructor(
    private val categoryRepository: CategoryRepository
) {

    operator fun invoke(): Flow<NetworkResponse<CategoryResponse>> = flow {
        try {
            emit(NetworkResponse.Loading())
            val response = categoryRepository.getCategories()
            emit(NetworkResponse.Success(data = response))
        } catch (e: HttpException) {
            emit(NetworkResponse.Error(errorMsg = e.localizedMessage ?: "Unexpected error !"))
        } catch (e: IOException) {
            emit(NetworkResponse.Error(errorMsg = "Please check your internet connection"))
        }
    }

}

