package com.parvez.superecommerce.domain.usecase

import com.parvez.superecommerce.data.model.login.LoginResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.data.repository.login.LoginRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val loginRepository: LoginRepository
) {

    operator fun invoke(
        username: String,
        password: String
    ): Flow<NetworkResponse<LoginResponse>> = flow {
        try {
            emit(NetworkResponse.Loading())
            val response: LoginResponse = loginRepository.login(username, password)
            emit(NetworkResponse.Success(data = response))
        }catch (e: HttpException){
            emit(NetworkResponse.Error(errorMsg = e.localizedMessage?: "Unexpected error !"))
        }catch (e: IOException){
            emit(NetworkResponse.Error(errorMsg = "Please check your internet connection"))
        }
    }

}