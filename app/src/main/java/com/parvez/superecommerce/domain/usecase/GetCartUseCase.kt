package com.parvez.superecommerce.domain.usecase

import android.util.Log
import com.parvez.superecommerce.data.model.cart.CartResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.data.repository.cart.CartRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetCartUseCase @Inject constructor(
    private val cartRepository: CartRepository
) {

    operator fun invoke(): Flow<NetworkResponse<CartResponse>> = flow {
        try {
            emit(NetworkResponse.Loading())
            val response = cartRepository.getCart()
            emit(NetworkResponse.Success(data = response))
        } catch (e: HttpException) {
            Log.d("TAG", "Log: 2 - Http exception")
            emit(NetworkResponse.Error(errorMsg = e.localizedMessage ?: "Unexpected error !"))
        } catch (e: IOException) {
            Log.d("TAG", "Log: 2 - IO Exception")
            emit(NetworkResponse.Error(errorMsg = "Please check your internet connection"))
        }
    }

}