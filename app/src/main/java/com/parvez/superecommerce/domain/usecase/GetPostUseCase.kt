package com.parvez.superecommerce.domain.usecase

import com.parvez.superecommerce.data.model.post.PostResponse
import com.parvez.superecommerce.data.remote.NetworkResponse
import com.parvez.superecommerce.data.repository.post.PostRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetPostUseCase @Inject constructor(
    private val postRepository: PostRepository
) {

    operator fun invoke(): Flow<NetworkResponse<PostResponse>> = flow {
        try {
            emit(NetworkResponse.Loading())
            val response = postRepository.getPost()
            emit(NetworkResponse.Success(data = response))
        } catch (e: HttpException) {
            emit(NetworkResponse.Error(errorMsg = e.localizedMessage ?: "Unexpected error !"))
        } catch (e: IOException) {
            emit(NetworkResponse.Error(errorMsg = "Please check your internet connection"))
        }
    }

}