package com.parvez.superecommerce

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SuperEcommerceApplication: Application() {
}