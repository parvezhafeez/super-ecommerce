package com.parvez.superecommerce.di

import com.parvez.superecommerce.data.remote.ApiInterface
import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSource
import com.parvez.superecommerce.data.repository.cart.remote.CartRemoteSourceImpl
import com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSource
import com.parvez.superecommerce.data.repository.category.remote.CategoryRemoteSourceImpl
import com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSource
import com.parvez.superecommerce.data.repository.login.remote.LoginRemoteSourceImpl
import com.parvez.superecommerce.data.repository.post.remote.PostRemoteSource
import com.parvez.superecommerce.data.repository.post.remote.PostRemoteSourceImpl
import com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSource
import com.parvez.superecommerce.data.repository.product.remote.ProductRemoteSourceImpl
import com.parvez.superecommerce.data.repository.test.remote.TestRemoteSource
import com.parvez.superecommerce.data.repository.test.remote.TestRemoteSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideTestRemoteSource(apiInterface: ApiInterface): TestRemoteSource {
        return TestRemoteSourceImpl(apiInterface)
    }

    @Singleton
    @Provides
    fun provideLoginRemoteSource(apiInterface: ApiInterface): LoginRemoteSource {
        return LoginRemoteSourceImpl(apiInterface)
    }

    @Singleton
    @Provides
    fun provideCategoryRemoteSource(apiInterface: ApiInterface): CategoryRemoteSource {
        return CategoryRemoteSourceImpl(apiInterface)
    }

    @Singleton
    @Provides
    fun provideProductRemoteSource(apiInterface: ApiInterface): ProductRemoteSource{
        return ProductRemoteSourceImpl(apiInterface)
    }

    @Singleton
    @Provides
    fun provideCartRemoteSource(apiInterface: ApiInterface): CartRemoteSource {
        return CartRemoteSourceImpl(apiInterface)
    }

    @Provides
    @Singleton
    fun providePostRemoteSource(apiInterface: ApiInterface): PostRemoteSource {
        return PostRemoteSourceImpl(apiInterface)
    }
}

